module gitlab.com/zalleon/payment-system

go 1.12

require (
	github.com/coreos/go-systemd v0.0.0-20190719114852-fd7a80b32e1f // indirect
	github.com/fasthttp/router v1.2.2
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/jackc/pgerrcode v0.0.0-20190803225404-afa3381909a6
	github.com/jackc/pgx/v4 v4.7.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/magiconair/properties v1.8.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/shopspring/decimal v0.0.0-20200227202807-02e2044944cc
	github.com/spf13/viper v1.7.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/timsolov/rest-query-parser v1.8.1
	github.com/valyala/fasthttp v1.14.0
	go.uber.org/multierr v1.5.0 // indirect
	go.uber.org/zap v1.10.0
)
