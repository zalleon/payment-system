package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/zalleon/payment-system/internal/services"

	"go.uber.org/zap"

	"github.com/fasthttp/router"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/log/zapadapter"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
	"github.com/valyala/fasthttp/pprofhandler"

	_ "gitlab.com/zalleon/payment-system/docs"
	"gitlab.com/zalleon/payment-system/internal/api/handlers"
	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/metrics"
	"gitlab.com/zalleon/payment-system/internal/persistence"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

func main() {
	cfg, err := config.Read()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to load config: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf("%+v\n", cfg)

	logger, _ := zap.NewProduction()
	defer func() {
		_ = logger.Sync()
	}()

	applicationMetrics := metrics.NewMetrics()
	applicationMetrics.MustRegister()

	dbConnectionPool := MustPostgres(&cfg, logger)
	defer dbConnectionPool.Close()

	wg := &sync.WaitGroup{}
	controlChan := make(chan struct{})

	// Run postgres connection pool metrics watcher
	wg.Add(1)
	go watchPoolMetrics(logger, dbConnectionPool, controlChan, applicationMetrics, wg)

	// Clean up connections after shutdown
	defer func() {
		if r := recover(); r != nil {
			logger.Error(fmt.Sprintf("App crashed & recovered with: %+v", r))
			close(controlChan)
			dbConnectionPool.Close()
		}
	}()

	jsonResponseWriter := handlers.NewJSONResponseWriter(logger)

	// Setup handlers dependencies
	currencyRateRepository := persistence.NewCurrencyRatePostgresRepository(dbConnectionPool)
	addCurrencyRateCommandHandler := usecase.NewAddCurrencyRateCommandHandler(currencyRateRepository)
	addCurrencyRateHandler := handlers.NewAddCurrencyRateHandler(&cfg, logger, jsonResponseWriter, addCurrencyRateCommandHandler)

	userRepository := persistence.NewUserPostgresRepository(dbConnectionPool)
	registerUserCommandHandler := usecase.NewRegisterUserCommandHandler(userRepository)
	registerUserHandler := handlers.NewRegisterUserHandler(&cfg, logger, jsonResponseWriter, registerUserCommandHandler)

	accountRepository := persistence.NewAccountPostgresRepository(dbConnectionPool)

	addAccountCommandHandler := usecase.NewAddAccountCommandHandler(userRepository, accountRepository)
	addAccountHandler := handlers.NewAddAccountHandler(&cfg, logger, jsonResponseWriter, addAccountCommandHandler)

	transactionsRepository := persistence.NewTransactionPostgresRepository(dbConnectionPool)

	txManager := persistence.NewTransactionManagerPostgres(dbConnectionPool)

	putMoneyCommandHandler := usecase.NewPutMoneyCommandHandler(logger, accountRepository, transactionsRepository, txManager)
	putMoneyHandler := handlers.NewPutMoneyHandler(&cfg, logger, jsonResponseWriter, putMoneyCommandHandler)

	currencyConverter := &services.CurrencyConverter{CurrencyRateRepository: currencyRateRepository}

	transferMoneyCommandHandler := usecase.NewTransferMoneyCommandHandler(logger, currencyConverter, accountRepository, transactionsRepository, txManager)
	transferMoneyHandler := handlers.NewTransferMoneyHandler(&cfg, logger, jsonResponseWriter, transferMoneyCommandHandler)

	getUsersQueryHandler := usecase.NewGetUsersQueryHandler(dbConnectionPool)
	getUsersHandler := handlers.NewGetUsersHandler(&cfg, logger, jsonResponseWriter, getUsersQueryHandler)

	getAccountsQueryHandler := usecase.NewGetAccountsQueryHandler(dbConnectionPool)
	getAccountsHandler := handlers.NewGetAccountsHandler(&cfg, logger, jsonResponseWriter, getAccountsQueryHandler)

	getUserTransactionsHandler := handlers.NewGetTransactionsReportHandler(&cfg, logger, dbConnectionPool)

	// Route => handler
	r := router.New()
	r.GET("/debug/pprof", pprofhandler.PprofHandler)
	r.GET("/metrics", fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler()))

	// swagger:route GET /users users listUsers
	//
	// Lists users filtered by some parameters.
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       200: GetUsersResponse
	r.GET("/users", getUsersHandler.Handle)

	//r.GET("/users/{ID}", nil) // TODO: implement  How to use ID := ctx.UserValue("ID")

	// swagger:route POST /users users createUser
	//
	// Register new user
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       400: ValidationError
	//       201:
	r.POST("/users", registerUserHandler.Handle)

	// swagger:route GET /accounts accounts listAccounts
	//
	// Lists accounts filtered by some parameters.
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       200: GetAccountsResponse
	r.GET("/accounts", getAccountsHandler.Handle)

	// r.GET("/accounts/{ID}", nil) // TODO: implement

	// swagger:route POST /accounts accounts createAccount
	//
	// Add new account to user
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       400: ValidationError
	//       201:
	r.POST("/accounts", addAccountHandler.Handle)

	// swagger:route GET /accounts/{ID}/transactions accounts listTransactions
	//
	// Lists account transactions filtered by some parameters.
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       200: ListAccountTransactionsResponse
	r.GET("/accounts/{ID}/transactions", getUserTransactionsHandler.Handle)

	// swagger:route POST /put-money accounts putMoney
	//
	// Put money to account
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       400: ValidationError
	//       201:
	r.POST("/put-money", putMoneyHandler.Handle)

	// swagger:route POST /transfer-money accounts transferMoney
	//
	// Transfer monet between accounts
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       400: ValidationError
	//       201:
	r.POST("/transfer-money", transferMoneyHandler.Handle)

	// swagger:route POST /currency-rates currency addCurrencyRate
	//
	// Add new currency rate
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	//
	//     Schemes: http, https
	//
	//     Responses:
	//       default: Error
	//       400: ValidationError
	//       200: GetUsersResponse
	r.POST("/currency-rates", addCurrencyRateHandler.Handle)

	// GET /reports - return all available reports
	// GET /reports/report-name - return specific report

	// Init http server
	s := &fasthttp.Server{
		Handler:     r.Handler,
		ReadTimeout: cfg.Server.ConnectionTimeout,
	}

	// Watch OS Signals
	wg.Add(1)
	go watchOSSignal(logger, controlChan, s, wg)

	// Start server
	logger.Info("Start http server")
	if err := s.ListenAndServe(fmt.Sprintf("%s:%d", cfg.Server.Connection.Host, cfg.Server.Connection.Port)); err != nil {
		logger.Fatal(fmt.Sprintf("Unable to start server: %v", err))
	}

	// Wait until metrics watcher and os signal watcher are down
	wg.Wait()

	logger.Info("Stop service")
}

func MustPostgres(cfg *config.Config, logger *zap.Logger) *pgxpool.Pool {
	pgxCfg, _ := pgx.ParseConfig("")
	pgxCfg.Host = cfg.Postgres.Connection.Host
	pgxCfg.Port = cfg.Postgres.Connection.Port
	pgxCfg.Database = cfg.Postgres.Database
	pgxCfg.User = cfg.Postgres.User
	pgxCfg.Password = cfg.Postgres.Password
	pgxCfg.Logger = zapadapter.NewLogger(logger)
	pgxCfg.LogLevel = pgx.LogLevelDebug
	pgxCfg.PreferSimpleProtocol = true

	pgxPoolCfg, _ := pgxpool.ParseConfig("")
	pgxPoolCfg.ConnConfig = pgxCfg
	pgxPoolCfg.MaxConns = cfg.Postgres.MaxConnections
	pgxPoolCfg.MinConns = cfg.Postgres.MinConnections

	dbConnectionPool, err := pgxpool.ConnectConfig(context.Background(), pgxPoolCfg)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Unable to connect to database: %v", err))
	}

	logger.Info("Successfully connected to database")

	return dbConnectionPool
}

func watchPoolMetrics(logger *zap.Logger, pool *pgxpool.Pool, stop chan struct{}, appMetrics *metrics.ApplicationMetrics, wg *sync.WaitGroup) {
	defer func() {
		wg.Done()
		logger.Info("Stop watch pool metrics")
	}()

	logger.Info("Start watch pool metrics")

	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			stat := pool.Stat()
			// TODO: replace with real build version
			build := "v0.0.1"
			appMetrics.PgxPoolMaxConns.WithLabelValues(build).Set(float64(stat.MaxConns()))
			appMetrics.PgxPoolIdleConns.WithLabelValues(build).Set(float64(stat.IdleConns()))
			appMetrics.PgxPoolTotalConns.WithLabelValues(build).Set(float64(stat.TotalConns()))
			appMetrics.PgxPoolAcquireCount.WithLabelValues(build).Set(float64(stat.AcquireCount()))
			appMetrics.PgxPoolAcquiredConns.WithLabelValues(build).Set(float64(stat.AcquiredConns()))
			appMetrics.PgxPoolAcquireDuration.WithLabelValues(build).Observe(stat.AcquireDuration().Seconds())
			appMetrics.PgxPoolCanceledAcquireCount.WithLabelValues(build).Set(float64(stat.CanceledAcquireCount()))
			appMetrics.PgxPoolConstructingConns.WithLabelValues(build).Set(float64(stat.ConstructingConns()))
			appMetrics.PgxPoolEmptyAcquireCount.WithLabelValues(build).Set(float64(stat.EmptyAcquireCount()))
		case _, ok := <-stop:
			if !ok {
				ticker.Stop()
				return
			}
		}
	}
}

func watchOSSignal(logger *zap.Logger, control chan struct{}, s *fasthttp.Server, wg *sync.WaitGroup) {
	defer func() {
		wg.Done()
		logger.Info("Stop watch os signal")
	}()

	logger.Info("Start watch os signal")

	termChannel := make(chan os.Signal, 1)
	signal.Notify(termChannel, syscall.SIGTERM, syscall.SIGINT)

	<-termChannel
	logger.Info("Receive term signal")

	close(control)
	logger.Info("Control chan closed")

	logger.Info("Shutdown server")
	logger.Info(fmt.Sprintf("Open connections %d", s.GetOpenConnectionsCount()))
	if err := s.Shutdown(); err != nil {
		logger.Error(err.Error())
	}
	logger.Info("Server down")
}
