module github.com/timsolov/rest-query-parser

go 1.13

require (
	github.com/josharian/impl v0.0.0-20191119165012-6b9658ad00c7 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/tools v0.0.0-20200515220128-d3bf790afa53 // indirect
)
