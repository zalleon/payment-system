DROP TABLE users;
DROP TABLE accounts;
DROP TABLE transactions;
DROP TABLE currency_rates;
DROP INDEX from_account_id_to_account_id_date_idx;