CREATE TABLE users
(
    id         UUID                        NOT NULL,
    name       VARCHAR                     NOT NULL,
    email      VARCHAR                     NOT NULL,
    country    VARCHAR                     NOT NULL,
    city       VARCHAR                     NOT NULL,
    created_at TIMESTAMP without time zone NOT NULL DEFAULT now(),
    updated_at TIMESTAMP without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id),
    UNIQUE(email)
);

CREATE TABLE accounts
(
    id         UUID                        NOT NULL,
    user_id    UUID                        NOT NULL,
    currency   VARCHAR(3)                  NOT NULL,
    balance    NUMERIC(12, 2)              NOT NULL,
    created_at TIMESTAMP without time zone NOT NULL DEFAULT now(),
    updated_at TIMESTAMP without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
);

CREATE TABLE transactions
(
    id                 UUID                        NOT NULL,
    date               TIMESTAMP without time zone NOT NULL DEFAULT now(),
    type               VARCHAR                     NOT NULL,
    from_account_id    UUID                        NOT NULL,
    from_currency      VARCHAR(3)                  NOT NULL,
    from_currency_rate NUMERIC(13, 10)             NOT NULL,
    from_amount        NUMERIC(12, 2)              NOT NULL,
    to_account_id      UUID                        NOT NULL,
    to_currency        VARCHAR(3)                  NOT NULL,
    to_currency_rate   NUMERIC(13, 10)             NOT NULL,
    to_amount          NUMERIC(12, 2)              NOT NULL,
    created_at         TIMESTAMP without time zone NOT NULL DEFAULT now(),
    updated_at         TIMESTAMP without time zone NOT NULL DEFAULT now(),
    PRIMARY KEY (id)
);
CREATE INDEX from_user_id_to_user_id_date_idx ON transactions(from_account_id,to_account_id,date);

CREATE TABLE currency_rates
(
    date     TIMESTAMP without time zone NOT NULL,
    currency VARCHAR(3)                  NOT NULL,
    rate     NUMERIC(13, 10)             NOT NULL,
    PRIMARY KEY (date, currency)
);