// Package services contains domain logic
package services

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/pkg/money"
)

//go:generate mockery --output ./../services/mocks --name CurrencyConverterInterface --structname CurrencyConverterMock --filename currency_converter_mock.go

type CurrencyConverterInterface interface {
	Convert(time.Time, *money.Money, *money.Currency) (*money.Money, error)
}

type CurrencyConverter struct {
	CurrencyRateRepository domain.CurrencyRateRepositoryInterface
}

func (c *CurrencyConverter) Convert(date time.Time, m *money.Money, curr *money.Currency) (*money.Money, error) {
	if m.Currency().Equal(curr) {
		return m, nil
	}

	if curr.Code == money.CodeUSD {
		rate, err := c.CurrencyRateRepository.Get(context.Background(), m.Currency().Code, date)
		if err != nil {
			return nil, errors.Wrapf(err, "unable to load currency rate. code %s date %v", m.Currency().Code, date)
		}
		return money.NewMoney(m.Amount().Div(rate.Rate).Round(int32(curr.Fraction)), curr), nil
	}

	fromRate, err := c.CurrencyRateRepository.Get(context.Background(), m.Currency().Code, date)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to load currency rate. code %s date %v", m.Currency().Code, date)
	}

	toRate, err := c.CurrencyRateRepository.Get(context.Background(), curr.Code, date)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to load currency rate. code %s date %v", m.Currency().Code, date)
	}

	return money.NewMoney(m.Amount().Div(fromRate.Rate).Mul(toRate.Rate).Round(int32(curr.Fraction)), curr), nil
}
