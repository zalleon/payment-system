package services_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/zalleon/payment-system/internal/services"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/mock"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/pkg/money"
)

// Currency rate source
// https://api.exchangeratesapi.io/latest?base=USD

func TestCurrencyConverter_ConvertSameCurrency(t *testing.T) {
	converter := services.CurrencyConverter{CurrencyRateRepository: &mocks.CurrencyRateRepositoryMock{}}

	rubCurr, _ := money.GetCurrency(money.CodeRUB)
	eurCurr, _ := money.GetCurrency(money.CodeEUR)
	usdCurr, _ := money.GetCurrency(money.CodeUSD)

	tcs := []struct {
		m          *money.Money
		toCurrency *money.Currency
	}{
		{money.NewMoney(decimal.NewFromInt(10), usdCurr), usdCurr},
		{money.NewMoney(decimal.NewFromInt(10), eurCurr), eurCurr},
		{money.NewMoney(decimal.NewFromInt(10), rubCurr), rubCurr},
	}

	for _, tc := range tcs {
		converted, err := converter.Convert(time.Now(), tc.m, tc.toCurrency)
		if err != nil {
			t.Errorf("Unexpected error %+v", err)
		} else {
			isEqual, errEqual := tc.m.Equals(converted)
			if errEqual != nil {
				t.Errorf("Unexpected error %+v", errEqual)
			} else if !isEqual {
				t.Errorf("Expect equal amount of money. Expect: %s actual: %s", tc.m.Amount().String(), converted.Amount().String())
			}
		}
	}
}

func TestCurrencyConverter_ConvertToBaseCurrency(t *testing.T) {
	eurRate := decimal.NewFromFloat(0.8482483671)

	eurCurr, _ := money.GetCurrency(money.CodeEUR)
	usdCurr, _ := money.GetCurrency(money.CodeUSD)

	currencyRateRepository := &mocks.CurrencyRateRepositoryMock{}
	currencyRateRepository.On(
		"Get",
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return(&domain.CurrencyRate{
		Date:     time.Now(),
		Currency: money.CodeEUR,
		Rate:     eurRate,
	}, nil)

	converter := services.CurrencyConverter{CurrencyRateRepository: currencyRateRepository}

	eurMoney := money.NewMoney(decimal.NewFromInt(10), eurCurr)

	expectMoney := money.NewMoney(eurMoney.Amount().Div(eurRate).Round(int32(usdCurr.Fraction)), usdCurr)

	convertedMoney, err := converter.Convert(time.Now(), eurMoney, usdCurr)
	if err != nil {
		t.Errorf("Unexpected error %+v", err)
	} else {
		isEqual, errEqual := convertedMoney.Equals(expectMoney)
		if errEqual != nil {
			t.Errorf("Unexpected error %+v", errEqual)
		} else if !isEqual {
			t.Errorf("Expect equal amount of money. Expect: %s actual: %s", expectMoney.Amount().String(), convertedMoney.Amount().String())
		}
		if !convertedMoney.Currency().Equal(usdCurr) {
			t.Errorf("Invalid currency. Expect: %s actual: %s", usdCurr.Code, convertedMoney.Currency().Code)
		}
	}
}

func TestCurrencyConverter_ConvertBetweenNotBaseCurrencies(t *testing.T) {
	eurRate := decimal.NewFromFloat(0.8482483671)
	rubRate := decimal.NewFromFloat(75.7967596912)

	rubCurr, _ := money.GetCurrency(money.CodeRUB)
	eurCurr, _ := money.GetCurrency(money.CodeEUR)

	currencyRateRepository := &mocks.CurrencyRateRepositoryMock{}
	currencyRateRepository.On(
		"Get",
		mock.Anything,
		mock.Anything,
		mock.Anything,
	).Return(func(ctx context.Context, currency string, date time.Time) *domain.CurrencyRate {
		switch currency {
		case money.CodeEUR:
			return &domain.CurrencyRate{
				Date:     date,
				Currency: money.CodeEUR,
				Rate:     eurRate,
			}
		case money.CodeRUB:
			return &domain.CurrencyRate{
				Date:     date,
				Currency: money.CodeRUB,
				Rate:     rubRate,
			}
		default:
			return nil
		}
	}, func(ctx context.Context, currency string, date time.Time) error {
		if currency != money.CodeEUR && currency != money.CodeRUB {
			return fmt.Errorf("currency rate for %s not found", currency)
		}
		return nil
	})

	converter := services.CurrencyConverter{CurrencyRateRepository: currencyRateRepository}

	mEUR := money.NewMoney(decimal.NewFromInt(10), eurCurr)

	expectRUBMoney := money.NewMoney(mEUR.Amount().Div(eurRate).Mul(rubRate).Round(int32(rubCurr.Fraction)), rubCurr)

	mRUB, err := converter.Convert(time.Now(), mEUR, rubCurr)
	if err != nil {
		t.Errorf("Unexpected error %+v", err)
	} else {
		isEqual, errEqual := mRUB.Equals(expectRUBMoney)
		if errEqual != nil {
			t.Errorf("Unexpected error %+v", errEqual)
		} else if !isEqual {
			t.Errorf("Expect equal amount of money. Expect: %s actual: %s", expectRUBMoney.Amount().String(), mRUB.Amount().String())
		}
		if !mRUB.Currency().Equal(rubCurr) {
			t.Errorf("Invalid currency. Expect: %s actual: %s", rubCurr.Code, mRUB.Currency().Code)
		}
	}
}
