// Package domain contains core domain models and interfaces
package domain

import (
	"context"

	"github.com/gofrs/uuid"
)

type User struct {
	ID      uuid.UUID
	Name    string
	Email   string
	Country string
	City    string
}

type Users []User

//go:generate mockery --output ./../persistence/mocks --name UserRepositoryInterface --structname UserRepositoryMock --filename user_repository_mock.go

// UserRepositoryInterface ...
type UserRepositoryInterface interface {
	Get(ctx context.Context, uuid uuid.UUID) (*User, error)
	Persist(ctx context.Context, user *User) error
	Remove(ctx context.Context, user *User) error
}
