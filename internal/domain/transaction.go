package domain

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"github.com/shopspring/decimal"
)

type Transaction struct {
	ID               uuid.UUID
	Date             time.Time
	Type             string
	FromAccountID    uuid.UUID
	FromCurrency     string
	FromCurrencyRate decimal.Decimal
	FromAmount       decimal.Decimal
	ToAccountID      uuid.UUID
	ToCurrency       string
	ToCurrencyRate   decimal.Decimal
	ToAmount         decimal.Decimal
}

type Transactions []Transaction

//go:generate mockery --output ./../persistence/mocks --name TransactionRepositoryInterface --structname TransactionRepositoryMock --filename transaction_repository_mock.go

// TransactionRepositoryInterface ...
type TransactionRepositoryInterface interface {
	Get(ctx context.Context, uuid uuid.UUID) (*Transaction, error)
	Persist(ctx context.Context, transaction *Transaction) error
	Remove(ctx context.Context, transaction *Transaction) error
}
