package domain

import (
	"context"
	"time"

	"github.com/shopspring/decimal"
)

type CurrencyRate struct {
	Date     time.Time
	Currency string
	Rate     decimal.Decimal
}

type CurrencyRates []CurrencyRate

//go:generate mockery --output ./../persistence/mocks --name CurrencyRateRepositoryInterface --structname CurrencyRateRepositoryMock --filename currency_rate_repository_mock.go

// CurrencyRateRepositoryInterface ...
type CurrencyRateRepositoryInterface interface {
	Get(ctx context.Context, currency string, date time.Time) (*CurrencyRate, error)
	Persist(ctx context.Context, rate *CurrencyRate) error
	Remove(ctx context.Context, rate *CurrencyRate) error
}
