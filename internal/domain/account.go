package domain

import (
	"context"

	"github.com/gofrs/uuid"

	"gitlab.com/zalleon/payment-system/pkg/money"
)

// Account ...
type Account struct {
	ID      uuid.UUID
	User    uuid.UUID
	Balance *money.Money
}

type Accounts []Account

//go:generate mockery --output ./../persistence/mocks --name AccountRepositoryInterface --structname AccountRepositoryMock --filename account_repository_mock.go

// AccountRepositoryInterface ...
type AccountRepositoryInterface interface {
	Get(ctx context.Context, id uuid.UUID) (*Account, error)
	Persist(ctx context.Context, account *Account) error
	Remove(ctx context.Context, account *Account) error
	UpdateBalance(ctx context.Context, account *Account) error
}
