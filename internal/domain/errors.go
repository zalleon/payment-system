package domain

import (
	"errors"
	"fmt"
)

var ErrEntityNotFound = errors.New("entity not found")
var ErrEntityNotUpdated = errors.New("entity not updated")
var ErrEntityExists = errors.New("entity exists")

type ValidationError struct {
	Field   string
	Message string
}

func (e *ValidationError) Error() string {
	return fmt.Sprintf("%s %s", e.Field, e.Message)
}
