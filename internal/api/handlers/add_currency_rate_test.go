package handlers

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/mock"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/internal/utils"
)

func TestAddCurrencyRateHandler_HandleEmptyBody(t *testing.T) {
	handler := NewAddCurrencyRateHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddCurrencyRateCommandHandler(
			&mocks.CurrencyRateRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`""`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format"}`)
}

func TestAddCurrencyRateHandler_HandleValidationError(t *testing.T) {
	handler := NewAddCurrencyRateHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddCurrencyRateCommandHandler(
			&mocks.CurrencyRateRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"date":"","currency":"RUB","rate":30.1234567809}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"cannot be blank","field":"date"}]}`)
}

func TestAddCurrencyRateHandler_HandleRateExists(t *testing.T) {
	rep := &mocks.CurrencyRateRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(domain.ErrEntityExists)

	handler := NewAddCurrencyRateHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddCurrencyRateCommandHandler(rep),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"date":"2020-01-02","currency":"RUB","rate":30.1234567809}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"entity exists","field":"currency rate"}]}`)
}

func TestAddCurrencyRateHandler_HandleInternalError(t *testing.T) {
	rep := &mocks.CurrencyRateRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(fmt.Errorf("internal error"))

	handler := NewAddCurrencyRateHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddCurrencyRateCommandHandler(rep),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"date":"2020-01-03","currency":"RUB","rate":30.1234567809}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 500)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Internal server error"}`)
}

func TestAddCurrencyRateHandler_HandleSuccess(t *testing.T) {
	rep := &mocks.CurrencyRateRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(func(ctx context.Context, rate *domain.CurrencyRate) error {
		r, _ := decimal.NewFromString("30.1234567809")
		if rate.Date.Format("2006-01-02") == "2020-01-01" &&
			rate.Currency == money.CodeRUB &&
			rate.Rate.Equal(r) {
			return nil
		}
		return fmt.Errorf("wrong currency rate payload")
	})

	handler := NewAddCurrencyRateHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddCurrencyRateCommandHandler(rep),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"date":"2020-01-01","currency":"RUB","rate":30.1234567809}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 201)
	assert.Equal(t, string(resp.Header.ContentType()), "text/plain; charset=utf-8")
	assert.Equal(t, string(resp.Body()), ``)
}
