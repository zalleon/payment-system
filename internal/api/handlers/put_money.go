package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	"go.uber.org/zap"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type putMoneyHandler struct {
	cfg            *config.Config
	logger         *zap.Logger
	responseWriter JSONResponseWriterInterface
	handler        *usecase.PutMoneyCommandHandler
}

func NewPutMoneyHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, handler *usecase.PutMoneyCommandHandler) *putMoneyHandler {
	return &putMoneyHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, handler: handler}
}

// PutMoneyRequest represents body of put money request.
//
// swagger:parameters putMoney
type PutMoneyRequest struct {
	Account string          `json:"account"`
	Amount  decimal.Decimal `json:"amount"`
}

func (h *putMoneyHandler) Handle(ctx *fasthttp.RequestCtx) {
	req := &PutMoneyRequest{}

	if err := json.Unmarshal(ctx.PostBody(), req); err != nil {
		h.logger.Error(fmt.Sprintf("Error while decode put money request: %s error: %s", ctx.PostBody(), err))
		h.responseWriter.WriteError(ctx, &Error{Message: MsgInvalidRequest}, fasthttp.StatusBadRequest)
		return
	}

	cmd, err := usecase.NewPutMoneyCommand(req.Account, req.Amount)
	if err != nil {
		h.responseWriter.WriteError(
			ctx,
			&ValidationError{
				Message: MsgInvalidRequest,
				Errors: []ValidationFieldError{{
					Message: err.Message,
					Field:   err.Field,
				}},
			},
			fasthttp.StatusBadRequest,
		)
		return
	}

	ctxH := context.Background()
	if err := h.handler.Handle(ctxH, cmd); err != nil {
		if err, ok := err.(*domain.ValidationError); ok {
			h.responseWriter.WriteError(
				ctx,
				&ValidationError{
					Message: MsgInvalidRequest,
					Errors: []ValidationFieldError{{
						Message: err.Message,
						Field:   err.Field,
					}},
				},
				fasthttp.StatusBadRequest,
			)
			return
		}

		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to handle put money command. Payload: %+v", cmd)),
		)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}
