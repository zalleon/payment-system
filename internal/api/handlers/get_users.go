package handlers

import (
	"fmt"

	"github.com/pkg/errors"

	"github.com/valyala/fasthttp"

	"go.uber.org/zap"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type getUsersHandler struct {
	cfg                  *config.Config
	logger               *zap.Logger
	responseWriter       JSONResponseWriterInterface
	getUsersQueryHandler *usecase.GetUsersQueryHandler
}

func NewGetUsersHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, getUsersQueryHandler *usecase.GetUsersQueryHandler) *getUsersHandler {
	return &getUsersHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, getUsersQueryHandler: getUsersQueryHandler}
}

// GetUsersResponse represents the user list
//
// swagger:model
type GetUsersResponse struct {
	Users []usecase.UserDto `json:"users"`
}

func (h *getUsersHandler) Handle(ctx *fasthttp.RequestCtx) {
	uri := ""
	if len(ctx.Request.Header.RequestURI()) > 0 {
		uri = string(ctx.Request.Header.RequestURI())
	}

	getUsersQuery, err := usecase.NewGetUsersQuery(uri)
	if err != nil {
		h.responseWriter.WriteError(ctx, &Error{Message: err.Error()}, fasthttp.StatusBadRequest)
		return
	}

	users, err := h.getUsersQueryHandler.Handle(ctx, getUsersQuery)
	if err != nil {
		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to execute users list query. Payload: %+v", getUsersQuery)),
		)
		return
	}

	h.responseWriter.WriteSuccess(ctx, &GetUsersResponse{Users: users}, fasthttp.StatusOK)
}
