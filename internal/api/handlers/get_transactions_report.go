package handlers

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"gitlab.com/zalleon/payment-system/internal/config"
	"go.uber.org/zap"
)

type getTransactionsReportHandler struct {
	cfg              *config.Config
	logger           *zap.Logger
	dbConnectionPool *pgxpool.Pool
}

func NewGetTransactionsReportHandler(cfg *config.Config, logger *zap.Logger, dbConnectionPool *pgxpool.Pool) *getTransactionsReportHandler {
	return &getTransactionsReportHandler{cfg: cfg, logger: logger, dbConnectionPool: dbConnectionPool}
}

type GetUserTransactionsResponse struct {
	User         string           `json:"user"`
	Transactions []TransactionDto `json:"transactions"`
	Total        decimal.Decimal  `json:"total"`
	TotalUsd     decimal.Decimal  `json:"totalUsd"`
}

type TransactionDto struct {
	ID        string          `json:"id"`
	Date      string          `json:"date"`
	Type      string          `json:"type"`
	Currency  string          `json:"currency"`
	Amount    decimal.Decimal `json:"amount"`
	AmountUsd decimal.Decimal `json:"amountUsd"`
}

func (h *getTransactionsReportHandler) Handle(ctx *fasthttp.RequestCtx) {
	ctx.Success(ContentTypeJSON, []byte(`{"message":"implementation not found"}`))
}
