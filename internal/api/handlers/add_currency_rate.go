package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"go.uber.org/zap"

	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type addCurrencyRateHandler struct {
	cfg            *config.Config
	logger         *zap.Logger
	responseWriter JSONResponseWriterInterface
	handler        *usecase.AddCurrencyRateCommandHandler
}

func NewAddCurrencyRateHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, handler *usecase.AddCurrencyRateCommandHandler) *addCurrencyRateHandler {
	return &addCurrencyRateHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, handler: handler}
}

// AddCurrencyRateRequest represents add currency rate request body
//
// swagger:parameters addCurrencyRate
type AddCurrencyRateRequest struct {
	Date     string
	Currency string
	Rate     decimal.Decimal
}

func (h *addCurrencyRateHandler) Handle(ctx *fasthttp.RequestCtx) {
	req := &AddCurrencyRateRequest{}

	if err := json.Unmarshal(ctx.PostBody(), req); err != nil {
		h.logger.Error(fmt.Sprintf("Error while decode add currency rate request: %s error: %s", ctx.PostBody(), err))
		h.responseWriter.WriteError(ctx, &Error{Message: MsgInvalidRequest}, fasthttp.StatusBadRequest)
		return
	}

	cmd, err := usecase.NewAddCurrencyRateCommand(req.Date, req.Currency, req.Rate)
	if err != nil {
		h.responseWriter.WriteError(
			ctx,
			&ValidationError{
				Message: MsgInvalidRequest,
				Errors: []ValidationFieldError{{
					Message: err.Message,
					Field:   err.Field,
				}},
			},
			fasthttp.StatusBadRequest,
		)
		return
	}

	ctxH := context.Background()
	if err := h.handler.Handle(ctxH, cmd); err != nil {
		if err, ok := err.(*domain.ValidationError); ok {
			h.responseWriter.WriteError(
				ctx,
				&ValidationError{
					Message: MsgInvalidRequest,
					Errors: []ValidationFieldError{{
						Message: err.Message,
						Field:   err.Field,
					}},
				},
				fasthttp.StatusBadRequest,
			)
			return
		}

		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to handle add currency rate command. Payload: %+v", cmd)),
		)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}
