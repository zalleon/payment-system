package handlers

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/gofrs/uuid"

	"github.com/magiconair/properties/assert"
	"github.com/stretchr/testify/mock"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/internal/utils"
)

func TestAddAccountHandler_HandleEmptyBody(t *testing.T) {
	handler := NewAddAccountHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddAccountCommandHandler(
			&mocks.UserRepositoryMock{},
			&mocks.AccountRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`""`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format"}`)
}

func TestAddAccountHandler_HandleValidationError(t *testing.T) {
	handler := NewAddAccountHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddAccountCommandHandler(
			&mocks.UserRepositoryMock{},
			&mocks.AccountRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"user":"","currency":""}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"cannot be blank","field":"user"}]}`)
}

func TestAddAccountHandler_HandleInternalError(t *testing.T) {
	userID, _ := uuid.NewV4()

	userRep := &mocks.UserRepositoryMock{}
	userRep.On(
		"Get",
		mock.Anything,
		mock.Anything,
	).Return(nil, fmt.Errorf("some intternal server error"))

	accountRep := &mocks.AccountRepositoryMock{}

	handler := NewAddAccountHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddAccountCommandHandler(
			userRep,
			accountRep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(fmt.Sprintf(`{"user":"%s","currency":"RUB"}`, userID.String())))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 500)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Internal server error"}`)
}

func TestAddAccountHandler_HandleUserDoesNotExist(t *testing.T) {
	userID, _ := uuid.NewV4()

	userRep := &mocks.UserRepositoryMock{}
	userRep.On(
		"Get",
		mock.Anything,
		mock.Anything,
	).Return(nil, domain.ErrEntityNotFound)

	accountRep := &mocks.AccountRepositoryMock{}

	handler := NewAddAccountHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddAccountCommandHandler(
			userRep,
			accountRep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(fmt.Sprintf(`{"user":"%s","currency":"RUB"}`, userID.String())))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"does not exist","field":"user"}]}`)
}

func TestAddAccountHandler_HandleSuccess(t *testing.T) {
	userID, _ := uuid.NewV4()

	userRep := &mocks.UserRepositoryMock{}
	userRep.On(
		"Get",
		mock.Anything,
		mock.Anything,
	).Return(nil, nil)

	accountRep := &mocks.AccountRepositoryMock{}
	accountRep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(func(ctx context.Context, account *domain.Account) error {
		if account.User == userID && account.Balance.Currency().Code == money.CodeRUB {
			return nil
		}
		return fmt.Errorf("wrong account payload")
	})

	handler := NewAddAccountHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewAddAccountCommandHandler(
			userRep,
			accountRep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(fmt.Sprintf(`{"user":"%s","currency":"RUB"}`, userID.String())))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 201)
	assert.Equal(t, string(resp.Header.ContentType()), "text/plain; charset=utf-8")
	assert.Equal(t, string(resp.Body()), ``)
}
