package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"go.uber.org/zap"

	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type addAccountHandler struct {
	cfg            *config.Config
	logger         *zap.Logger
	responseWriter JSONResponseWriterInterface
	handler        *usecase.AddAccountCommandHandler
}

func NewAddAccountHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, handler *usecase.AddAccountCommandHandler) *addAccountHandler {
	return &addAccountHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, handler: handler}
}

// AddAccountRequest represents body of create account request.
//
// swagger:parameters createAccount
type AddAccountRequest struct {
	User     string `json:"user"`
	Currency string `json:"currency"`
}

// Handle create account request handler
func (h *addAccountHandler) Handle(ctx *fasthttp.RequestCtx) {
	req := &AddAccountRequest{}

	if err := json.Unmarshal(ctx.PostBody(), req); err != nil {
		h.logger.Error(fmt.Sprintf("Error while decode request: %s error: %s", ctx.PostBody(), err))
		h.responseWriter.WriteError(ctx, &Error{Message: MsgInvalidRequest}, fasthttp.StatusBadRequest)
		return
	}

	cmd, err := usecase.NewAddAccountCommand(req.User, req.Currency)
	if err != nil {
		h.responseWriter.WriteError(
			ctx,
			&ValidationError{
				Message: MsgInvalidRequest,
				Errors: []ValidationFieldError{{
					Message: err.Message,
					Field:   err.Field,
				}},
			},
			fasthttp.StatusBadRequest,
		)
		return
	}

	ctxH := context.Background()
	if err := h.handler.Handle(ctxH, cmd); err != nil {
		if err, ok := err.(*domain.ValidationError); ok {
			h.responseWriter.WriteError(
				ctx,
				&ValidationError{
					Message: MsgInvalidRequest,
					Errors: []ValidationFieldError{{
						Message: err.Message,
						Field:   err.Field,
					}},
				},
				fasthttp.StatusBadRequest,
			)
			return
		}

		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to handle add account command. Payload: %+v", cmd)),
		)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}
