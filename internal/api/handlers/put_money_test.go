package handlers

import (
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/valyala/fasthttp"
	"gitlab.com/zalleon/payment-system/internal/domain"

	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/internal/utils"
)

func TestPutMoneyHandler_HandleEmptyBody(t *testing.T) {
	handler := NewPutMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewPutMoneyCommandHandler(
			logger,
			&mocks.AccountRepositoryMock{},
			&mocks.TransactionRepositoryMock{},
			&mocks.TxManagerMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`""`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format"}`)
}

func TestPutMoneyHandler_HandleValidationError(t *testing.T) {
	handler := NewPutMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewPutMoneyCommandHandler(
			logger,
			&mocks.AccountRepositoryMock{},
			&mocks.TransactionRepositoryMock{},
			&mocks.TxManagerMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.Header.SetContentType("application/json")

	req.SetBody([]byte(`{"account":"","amount":100.5}`))

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"cannot be blank","field":"account"}]}`)
}

func TestPutMoneyHandler_HandleInternalServerError(t *testing.T) {
	accountID, _ := uuid.NewV4()

	txManager := &mocks.TxManagerMock{}
	txManager.On(
		"Begin",
		mock.Anything,
	).Return(nil, fmt.Errorf("unable to begin transaction"))

	handler := NewPutMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewPutMoneyCommandHandler(
			logger,
			&mocks.AccountRepositoryMock{},
			&mocks.TransactionRepositoryMock{},
			txManager,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(fmt.Sprintf(`{"account":"%s","amount":100.5}`, accountID.String())))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 500)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Internal server error"}`)
}

func TestPutMoneyHandler_HandleAccountDoesNotExist(t *testing.T) {
	accountID, _ := uuid.NewV4()

	accountRep := &mocks.AccountRepositoryMock{}
	accountRep.On(
		"Get",
		mock.Anything,
		mock.Anything,
	).Return(nil, domain.ErrEntityNotFound)

	tx := &mocks.TxMock{}
	tx.On("Conn").Return(nil)
	tx.On("IsCommit").Return(false)
	tx.On("Rollback", mock.Anything).Return(nil)

	txManager := &mocks.TxManagerMock{}
	txManager.On("Begin", mock.Anything).Return(tx, nil)

	handler := NewPutMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewPutMoneyCommandHandler(
			logger,
			accountRep,
			&mocks.TransactionRepositoryMock{},
			txManager,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(fmt.Sprintf(`{"account":"%s","amount":100.5}`, accountID.String())))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"does not exist","field":"account"}]}`)
}
