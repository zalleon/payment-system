package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"go.uber.org/zap"

	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type registerUserHandler struct {
	cfg            *config.Config
	logger         *zap.Logger
	responseWriter JSONResponseWriterInterface
	handler        *usecase.RegisterUserCommandHandler
}

func NewRegisterUserHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, handler *usecase.RegisterUserCommandHandler) *registerUserHandler {
	return &registerUserHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, handler: handler}
}

// RegisterUserRequest represents body of register user request.
//
// swagger:parameters createUser
type RegisterUserRequest struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Country string `json:"country"`
	City    string `json:"city"`
}

func (h *registerUserHandler) Handle(ctx *fasthttp.RequestCtx) {
	req := &RegisterUserRequest{}

	if err := json.Unmarshal(ctx.PostBody(), req); err != nil {
		h.logger.Error(fmt.Sprintf("Error while decode register user request: %s error: %s", ctx.PostBody(), err))
		h.responseWriter.WriteError(ctx, &Error{Message: MsgInvalidRequest}, fasthttp.StatusBadRequest)
		return
	}

	cmd, err := usecase.NewRegisterUserCommand(req.Name, req.Email, req.Country, req.City)
	if err != nil {
		h.responseWriter.WriteError(
			ctx,
			&ValidationError{
				Message: MsgInvalidRequest,
				Errors: []ValidationFieldError{{
					Message: err.Message,
					Field:   err.Field,
				}},
			},
			fasthttp.StatusBadRequest,
		)
		return
	}

	ctxH := context.Background()
	if err := h.handler.Handle(ctxH, cmd); err != nil {
		if err, ok := err.(*domain.ValidationError); ok {
			h.responseWriter.WriteError(
				ctx,
				&ValidationError{
					Message: MsgInvalidRequest,
					Errors: []ValidationFieldError{{
						Message: err.Message,
						Field:   err.Field,
					}},
				},
				fasthttp.StatusBadRequest,
			)
			return
		}

		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to handle register user command. Payload: %+v", cmd)),
		)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}
