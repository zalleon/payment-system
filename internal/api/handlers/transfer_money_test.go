package handlers

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"

	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/utils"
)

func TestTransferMoneyHandler_HandleWithEmptyBody(t *testing.T) {
	handler := NewTransferMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		nil,
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`""`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format"}`)
}

func TestTransferMoneyHandler_HandleWithWrongCommandParam(t *testing.T) {
	handler := NewTransferMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		nil,
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"from":"a57d592e-0975-42bc-8857-330341748d2d","to":"293a71be-6f26-4b51-be09-b6ac3b776907","money":{"amount":0,"currency":"RUB"}}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"should be greater than 0","field":"money"}]}`)
}

func TestTransferMoneyHandler_HandleWithValidationError(t *testing.T) {
	handler := NewTransferMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		nil,
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"from":"a57d592e-0975-42bc-8857-330341748d2d","to":"a57d592e-0975-42bc-8857-330341748d2d","money":{"amount":10,"currency":"RUB"}}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"from account and to account mut be different","field":"from"}]}`)
}

func TestTransferMoneyHandler_HandleWith500Error(t *testing.T) {
	txManager := &mocks.TxManagerMock{}
	txManager.On("Begin", mock.Anything).Return(nil, fmt.Errorf("unable to begin transaction"))
	handler := NewTransferMoneyHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewTransferMoneyCommandHandler(nil, nil, nil, nil, txManager),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"from":"a57d592e-0975-42bc-8857-330341748d2d","to":"b57d592e-0975-42bc-8857-330341748d2d","money":{"amount":10,"currency":"RUB"}}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 500)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Internal server error"}`)
}
