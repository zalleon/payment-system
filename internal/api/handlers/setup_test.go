package handlers

import (
	"os"
	"testing"

	"go.uber.org/zap"
)

var logger *zap.Logger

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	logger, _ = zap.NewProduction()
	defer func() {
		_ = logger.Sync()
	}()

	return m.Run()
}
