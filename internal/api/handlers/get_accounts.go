package handlers

import (
	"fmt"

	"github.com/pkg/errors"

	"github.com/valyala/fasthttp"

	"go.uber.org/zap"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type getAccountsHandler struct {
	cfg                     *config.Config
	logger                  *zap.Logger
	responseWriter          JSONResponseWriterInterface
	getAccountsQueryHandler *usecase.GetAccountsQueryHandler
}

func NewGetAccountsHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, getAccountsQueryHandler *usecase.GetAccountsQueryHandler) *getAccountsHandler {
	return &getAccountsHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, getAccountsQueryHandler: getAccountsQueryHandler}
}

// GetAccountsResponse ...
//
// swagger:model
type GetAccountsResponse struct {
	Accounts []usecase.AccountDto `json:"accounts"`
}

func (h *getAccountsHandler) Handle(ctx *fasthttp.RequestCtx) {
	uri := ""
	if len(ctx.Request.Header.RequestURI()) > 0 {
		uri = string(ctx.Request.Header.RequestURI())
	}

	getAccountsQuery, err := usecase.NewGetAccountsQuery(uri)
	if err != nil {
		h.responseWriter.WriteError(ctx, &Error{Message: err.Error()}, fasthttp.StatusBadRequest)
		return
	}

	accounts, err := h.getAccountsQueryHandler.Handle(ctx, getAccountsQuery)
	if err != nil {
		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to execute accounts list query. Payload: %+v", getAccountsQuery)),
		)
		return
	}

	h.responseWriter.WriteSuccess(ctx, &GetAccountsResponse{Accounts: accounts}, fasthttp.StatusOK)
}
