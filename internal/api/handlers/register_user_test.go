package handlers

import (
	"context"
	"fmt"
	"testing"

	"github.com/magiconair/properties/assert"
	"github.com/stretchr/testify/mock"
	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/internal/utils"
)

func TestRegisterUserHandler_HandleEmptyBody(t *testing.T) {
	handler := NewRegisterUserHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewRegisterUserCommandHandler(
			&mocks.UserRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`""`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format"}`)
}

func TestRegisterUserHandler_HandleValidationError(t *testing.T) {
	handler := NewRegisterUserHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewRegisterUserCommandHandler(
			&mocks.UserRepositoryMock{},
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"name":"","email":"","country":"","city":""}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"cannot be blank","field":"name"}]}`)
}

func TestRegisterUserHandler_HandleUserExists(t *testing.T) {
	rep := &mocks.UserRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(domain.ErrEntityExists)

	handler := NewRegisterUserHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewRegisterUserCommandHandler(
			rep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"name":"TestName","email":"test@email.com","country":"Russia","city":"Novosibirsk"}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 400)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Invalid request format","errors":[{"message":"entity exists","field":"user"}]}`)
}

func TestRegisterUserHandler_HandleInternalError(t *testing.T) {
	rep := &mocks.UserRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(fmt.Errorf("some intternal server error"))

	handler := NewRegisterUserHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewRegisterUserCommandHandler(
			rep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"name":"TestName","email":"test@email.com","country":"Russia","city":"Novosibirsk"}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 500)
	assert.Equal(t, string(resp.Header.ContentType()), "application/json")
	assert.Equal(t, string(resp.Body()), `{"message":"Internal server error"}`)
}

func TestRegisterUserHandler_HandleSuccess(t *testing.T) {
	rep := &mocks.UserRepositoryMock{}
	rep.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(func(ctx context.Context, user *domain.User) error {
		if user.Name == "TestName" &&
			user.Country == "Russia" &&
			user.City == "Novosibirsk" &&
			user.Email == "test@mail.com" {
			return nil
		}
		return fmt.Errorf("wrong user payload")
	})

	handler := NewRegisterUserHandler(
		nil,
		logger,
		NewJSONResponseWriter(logger),
		usecase.NewRegisterUserCommandHandler(
			rep,
		),
	)

	req := fasthttp.AcquireRequest()
	req.Header.SetMethod("POST")
	req.Header.SetHost("localhost")
	req.SetBody([]byte(`{"name":"TestName","email":"test@mail.com","country":"Russia","city":"Novosibirsk"}`))
	req.Header.SetContentType("application/json")

	resp := fasthttp.AcquireResponse()

	err := utils.Serve(handler.Handle, req, resp)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, resp.StatusCode(), 201)
	assert.Equal(t, string(resp.Header.ContentType()), "text/plain; charset=utf-8")
	assert.Equal(t, string(resp.Body()), ``)
}
