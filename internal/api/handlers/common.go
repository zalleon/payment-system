// Package handlers contains http api handlers
//
// Filtering:
// GET /users?name=Alex&creation_date=gte:2019-11-11
//
// Projection:
// GET /users?fields=name,country
//
// Sorting:
// GET /users?sort=-name,+country
//
// Paging:
// GET /users?limit=100&offset=10
//
// HTTP header X-Total-Count: 10
// HTTP header Link: <https://blog.mwaysolutions.com/sample/api/v1/cars?offset=15&limit=5>; rel="next",
// <https://blog.mwaysolutions.com/sample/api/v1/cars?offset=50&limit=3>; rel="last",
// <https://blog.mwaysolutions.com/sample/api/v1/cars?offset=0&limit=5>; rel="first",
// <https://blog.mwaysolutions.com/sample/api/v1/cars?offset=5&limit=5>; rel="prev",
//
// Success HTTP codes
// 200 – OK – Everything is working
// 201 – OK – New resource has been created
// 204 – OK – The resource was successfully deleted
// 304 – Not Modified – The client can use cached data
// 400 – Bad Request – The request was invalid or cannot be served. The exact error should be explained in the error payload. E.g. „The JSON is not valid“
// 401 – Unauthorized – The request requires an user authentication
// 403 – Forbidden – The server understood the request, but is refusing it or the access is not allowed.
// 404 – Not found – There is no resource behind the URI.
// 422 – Unprocessable Entity – Should be used if the server cannot process the enitity, e.g. if an image cannot be formatted or mandatory fields are missing in the payload.
// 500 – Internal Server Error – API developers should avoid this error. If an error occurs in the global catch blog, the stracktrace should be logged and not returned as response.
package handlers

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"go.uber.org/zap"

	"github.com/valyala/fasthttp"
)

const (
	MsgInvalidRequest = "Invalid request format"

	OkBody                  = `{"message":"OK"}`
	InternalServerErrorBody = `{"message":"Internal server error"}`

	ContentTypeJSON = "application/json"
)

// Error basic error response
//
// swagger:model Error
type Error struct {
	Message string `json:"message"`
}

// ValidationError validation error response
//
// swagger:model ValidationError
type ValidationError struct {
	Message string                 `json:"message"`
	Errors  []ValidationFieldError `json:"errors"`
}

// ValidationFieldError validation error for field
//
// swagger:model ValidationFieldError
type ValidationFieldError struct {
	Message string `json:"message"`
	Field   string `json:"field"`
}

type HTTPResponseWriterInterface interface {
	Error(msg string, statusCode int)
	Success(contentType string, body []byte)
	SetContentType(contentType string)
	SetStatusCode(statusCode int)
}

type JSONResponseWriterInterface interface {
	WriteError(w HTTPResponseWriterInterface, msg interface{}, statusCode int)
	WriteError500(w HTTPResponseWriterInterface, err error)
	WriteSuccess(w HTTPResponseWriterInterface, msg interface{}, statusCode int)
}

type JSONResponseWriter struct {
	logger *zap.Logger
}

func NewJSONResponseWriter(logger *zap.Logger) *JSONResponseWriter {
	return &JSONResponseWriter{logger: logger}
}

func (c *JSONResponseWriter) WriteError(w HTTPResponseWriterInterface, msg interface{}, statusCode int) {
	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		c.WriteError500(w, errors.Wrap(err, fmt.Sprintf("error while marshal response: %+v", msg)))
		return
	}

	w.Error(string(jsonMsg), statusCode)
	w.SetContentType(ContentTypeJSON)
}

func (c *JSONResponseWriter) WriteSuccess(w HTTPResponseWriterInterface, msg interface{}, statusCode int) {
	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		c.WriteError500(w, errors.Wrap(err, fmt.Sprintf("error while marshal response: %+v", msg)))
		return
	}

	w.Success(ContentTypeJSON, jsonMsg)
	w.SetStatusCode(statusCode)
}

func (c *JSONResponseWriter) WriteError500(w HTTPResponseWriterInterface, err error) {
	c.logger.Error(err.Error())

	w.Error(InternalServerErrorBody, fasthttp.StatusInternalServerError)
	w.SetContentType(ContentTypeJSON)
}
