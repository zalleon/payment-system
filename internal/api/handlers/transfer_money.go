package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/pkg/errors"

	"go.uber.org/zap"

	"github.com/valyala/fasthttp"

	"gitlab.com/zalleon/payment-system/internal/config"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type transferMoneyHandler struct {
	cfg            *config.Config
	logger         *zap.Logger
	responseWriter JSONResponseWriterInterface
	handler        *usecase.TransferMoneyCommandHandler
}

func NewTransferMoneyHandler(cfg *config.Config, logger *zap.Logger, responseWriter JSONResponseWriterInterface, handler *usecase.TransferMoneyCommandHandler) *transferMoneyHandler {
	return &transferMoneyHandler{cfg: cfg, logger: logger, responseWriter: responseWriter, handler: handler}
}

// TransferMoneyRequest represents body of transfer money request.
//
// swagger:parameters transferMoney
type TransferMoneyRequest struct {
	From  string       `json:"from"`
	To    string       `json:"to"`
	Money *money.Money `json:"money"`
}

func (h *transferMoneyHandler) Handle(ctx *fasthttp.RequestCtx) {
	req := &TransferMoneyRequest{}

	if err := json.Unmarshal(ctx.PostBody(), req); err != nil {
		h.logger.Error(fmt.Sprintf("Error while decode request: %s error: %s", ctx.PostBody(), err))
		h.responseWriter.WriteError(ctx, &Error{Message: MsgInvalidRequest}, fasthttp.StatusBadRequest)
		return
	}

	cmd, err := usecase.NewTransferMoneyCommand(req.From, req.To, req.Money)
	if err != nil {
		h.responseWriter.WriteError(
			ctx,
			&ValidationError{
				Message: MsgInvalidRequest,
				Errors: []ValidationFieldError{{
					Message: err.Message,
					Field:   err.Field,
				}},
			},
			fasthttp.StatusBadRequest,
		)
		return
	}

	ctxH := context.Background()
	if err := h.handler.Handle(ctxH, cmd); err != nil {
		if err, ok := err.(*domain.ValidationError); ok {
			h.responseWriter.WriteError(
				ctx,
				&ValidationError{
					Message: MsgInvalidRequest,
					Errors: []ValidationFieldError{{
						Message: err.Message,
						Field:   err.Field,
					}},
				},
				fasthttp.StatusBadRequest,
			)
			return
		}

		h.responseWriter.WriteError500(
			ctx,
			errors.Wrap(err, fmt.Sprintf("Unable to handle transfer money command. Command payload: %+v", cmd)),
		)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}
