// Package config contains application configs
package config

import (
	"strings"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	Server   server
	Postgres postgres
}

type server struct {
	Connection        hostPort
	ConnectionTimeout time.Duration
}

type postgres struct {
	Connection     hostPort
	User           string
	Password       string
	Database       string
	MaxConnections int32
	MinConnections int32
}

type hostPort struct {
	Host string
	Port uint16
}

func Read() (Config, error) {
	viper.SetEnvPrefix("PS")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	viper.SetDefault("server.connection.host", "")
	viper.SetDefault("server.connection.port", "8080")
	viper.SetDefault("server.connectionTimeout", 5*time.Second)

	viper.SetDefault("postgres.connection.host", "127.0.0.1")
	viper.SetDefault("postgres.connection.port", "5433")
	viper.SetDefault("postgres.user", "postgres")
	viper.SetDefault("postgres.password", "postgres")
	viper.SetDefault("postgres.database", "postgres")
	viper.SetDefault("postgres.MaxConnections", 10)
	viper.SetDefault("postgres.MinConnections", 1)

	cfg := Config{}
	err := viper.Unmarshal(&cfg)

	return cfg, err
}
