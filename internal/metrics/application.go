// Package metrics contains application metrics configuration
package metrics

import "github.com/prometheus/client_golang/prometheus"

type ApplicationMetrics struct {
	RequestTotal *prometheus.CounterVec

	PgxPoolTotalConns           *prometheus.GaugeVec
	PgxPoolConstructingConns    *prometheus.GaugeVec
	PgxPoolMaxConns             *prometheus.GaugeVec
	PgxPoolIdleConns            *prometheus.GaugeVec
	PgxPoolAcquireCount         *prometheus.GaugeVec
	PgxPoolAcquiredConns        *prometheus.GaugeVec
	PgxPoolAcquireDuration      *prometheus.HistogramVec
	PgxPoolCanceledAcquireCount *prometheus.GaugeVec
	PgxPoolEmptyAcquireCount    *prometheus.GaugeVec
}

func NewMetrics() *ApplicationMetrics {
	requestTotal := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_request_total",
			Help: "Total http requests",
		},
		[]string{"status", "method"},
	)

	pgxPoolTotalConns := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_total_conns", Help: ""}, []string{"build"})
	pgxPoolConstructingConns := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_constructing_conns", Help: ""}, []string{"build"})
	pgxPoolMaxConns := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_max_conns", Help: ""}, []string{"build"})
	pgxPoolIdleConns := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_idle_conns", Help: ""}, []string{"build"})
	pgxPoolAcquireCount := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_acquire_count", Help: ""}, []string{"build"})
	pgxPoolAcquiredConns := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_acquired_conns", Help: ""}, []string{"build"})
	pgxPoolAcquireDuration := prometheus.NewHistogramVec(prometheus.HistogramOpts{Name: "pgx_pool_acquire_duration", Help: ""}, []string{"build"})
	pgxPoolCanceledAcquireCount := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_canceled_acquire_count", Help: ""}, []string{"build"})
	pgxPoolEmptyAcquireCount := prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: "pgx_pool_empty_acquire_count", Help: ""}, []string{"build"})

	return &ApplicationMetrics{
		RequestTotal:                requestTotal,
		PgxPoolTotalConns:           pgxPoolTotalConns,
		PgxPoolConstructingConns:    pgxPoolConstructingConns,
		PgxPoolMaxConns:             pgxPoolMaxConns,
		PgxPoolIdleConns:            pgxPoolIdleConns,
		PgxPoolAcquireCount:         pgxPoolAcquireCount,
		PgxPoolAcquiredConns:        pgxPoolAcquiredConns,
		PgxPoolAcquireDuration:      pgxPoolAcquireDuration,
		PgxPoolCanceledAcquireCount: pgxPoolCanceledAcquireCount,
		PgxPoolEmptyAcquireCount:    pgxPoolEmptyAcquireCount,
	}
}

func (m *ApplicationMetrics) MustRegister() {
	prometheus.MustRegister(
		m.RequestTotal,
		m.PgxPoolTotalConns,
		m.PgxPoolConstructingConns,
		m.PgxPoolMaxConns,
		m.PgxPoolIdleConns,
		m.PgxPoolAcquireCount,
		m.PgxPoolAcquiredConns,
		m.PgxPoolAcquireDuration,
		m.PgxPoolCanceledAcquireCount,
		m.PgxPoolEmptyAcquireCount,
	)
}
