// +build integration

package persistence

import (
	"context"
	"fmt"
	"gitlab.com/zalleon/payment-system/pkg/money"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

func TestAccountPostgresRepository_GetWithoutTransaction(t *testing.T) {
	accountRepository := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()

	expectError := fmt.Errorf("unable to exec method without transaction")

	_, err := accountRepository.Get(context.Background(), id)
	if err != nil {
		if err.Error() != expectError.Error() {
			t.Errorf("Recive unexpected error %v", err)
		}
	} else {
		t.Errorf("Expect error %v", expectError)
	}
}

func TestAccountPostgresRepository_UpdateWithoutTransaction(t *testing.T) {
	accountRepository := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	account := &domain.Account{
		ID:      id,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}

	expectError := fmt.Errorf("unable to exec method without transaction")
	if err := accountRepository.UpdateBalance(context.Background(), account); err != nil {
		if err.Error() != expectError.Error() {
			t.Errorf("Recive unexpected error %v", err)
		}
	} else {
		t.Errorf("Expect error %v", expectError)
	}
}

func TestAccountPostgresRepository_UpdateUndefinedAccount(t *testing.T) {
	accountRepository := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	account := &domain.Account{
		ID:      id,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}
	ctx := context.Background()

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}
	defer func() {
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	if err := accountRepository.UpdateBalance(ctx, account); err != domain.ErrEntityNotUpdated {
		t.Errorf("Expect error %v", domain.ErrEntityNotUpdated)
	}
}

func TestAccountPostgresRepository_Persist_Duplicate(t *testing.T) {
	ctx := context.Background()

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}

	defer func() {
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	rep := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	account := &domain.Account{
		ID:      id,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}

	if err := rep.Persist(ctx, account); err != nil {
		t.Fatalf("Got error while persist account. %v", err)
	}

	if err := rep.Persist(ctx, account); err != nil {
		if err != domain.ErrEntityExists {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityExists))
	}
}

func TestAccountPostgresRepository_PersistGetRemoveGetNotFound(t *testing.T) {
	ctx := context.Background()

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}
	defer func() {
		if tx.IsCommit() {
			return
		}
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	accountRepository := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	account := &domain.Account{
		ID:      id,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}

	if err := accountRepository.Persist(ctx, account); err != nil {
		t.Fatalf("Got error while persist account. %v", err)
	}

	accountFromDB, err := accountRepository.Get(ctx, id)
	if err != nil {
		t.Fatalf("Unable to get account. %v", err)
	}

	assert.Equal(t, accountFromDB.User, account.User)
	assert.Equal(t, accountFromDB.Balance.Currency().Code, account.Balance.Currency().Code)

	isEqual, err := account.Balance.Equals(accountFromDB.Balance)
	if err != nil {
		t.Fatalf("Got error while compare balances. %v", err)
	}
	if !isEqual {
		t.Errorf("Expect %s got %s", account.Balance.Amount().String(), accountFromDB.Balance.Amount().String())
	}

	if err = tx.Commit(ctx); err != nil {
		t.Errorf("Got error while commit transaction. %v", err)
	}

	if err := accountRepository.Remove(ctx, account); err != nil {
		t.Fatalf("Got error while remove account. %v", err)
	}

	_, err = accountRepository.Get(ctx, id)
	if err != nil {
		if err != domain.ErrEntityNotFound {
			t.Fatalf("Unexpected error: %v", err)
		}
	} else {
		t.Errorf("Expect error: %v", domain.ErrEntityNotFound)
	}
}

func TestAccountPostgresRepository_PersistGetUpdate(t *testing.T) {
	ctx := context.Background()

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}

	defer func() {
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	accountRepository := NewAccountPostgresRepository(db)

	id, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	account := &domain.Account{
		ID:      id,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}

	if err := accountRepository.Persist(ctx, account); err != nil {
		t.Fatalf("Unable to persist account. %v", err)
	}

	account.Balance, err = account.Balance.Add(money.NewMoney(decimal.NewFromInt(30), curr))
	if err != nil {
		t.Errorf("Got error while add money %v", err)
	}

	if err := accountRepository.UpdateBalance(ctx, account); err != nil {
		t.Errorf("Got error while update account balance. %v", err)
	}

	accountFromDB, err := accountRepository.Get(ctx, id)
	if err != nil {
		t.Fatalf("Unable to get account. %v", err)
	}

	assert.Equal(t, accountFromDB.User, account.User)
	assert.Equal(t, accountFromDB.Balance.Currency().Code, account.Balance.Currency().Code)

	isEqual, err := account.Balance.Equals(accountFromDB.Balance)
	if err != nil {
		t.Fatalf("Got error while compare balances. %v", err)
	}
	if !isEqual {
		t.Errorf("Expect %s got %s", account.Balance.Amount().String(), accountFromDB.Balance.Amount().String())
	}
}
