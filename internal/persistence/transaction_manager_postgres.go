package persistence

import (
	"context"

	"github.com/jackc/pgx/v4"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type transactionManagerPostgres struct {
	dbConnectionPool *pgxpool.Pool
}

func NewTransactionManagerPostgres(dbConnectionPool *pgxpool.Pool) *transactionManagerPostgres {
	return &transactionManagerPostgres{dbConnectionPool: dbConnectionPool}
}

func (m *transactionManagerPostgres) Begin(ctx context.Context) (usecase.TxInterface, error) {
	tx, err := m.dbConnectionPool.Begin(ctx)
	if err != nil {
		return nil, err
	}

	return NewTransactionPostgres(tx), nil
}

type transactionPostgres struct {
	tx       pgx.Tx
	isCommit bool
}

func NewTransactionPostgres(tx pgx.Tx) *transactionPostgres {
	return &transactionPostgres{tx: tx, isCommit: false}
}

func (t *transactionPostgres) Commit(ctx context.Context) error {
	if err := t.tx.Commit(ctx); err != nil {
		return err
	}
	t.isCommit = true
	return nil
}

func (t *transactionPostgres) Rollback(ctx context.Context) error {
	return t.tx.Rollback(ctx)
}

func (t *transactionPostgres) Conn() interface{} {
	return t.tx.Conn()
}

func (t *transactionPostgres) IsCommit() bool {
	return t.isCommit
}
