// +build integration

package persistence

import (
	"context"
	"fmt"
	"os"
	"testing"

	"go.uber.org/zap"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/log/zapadapter"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/zalleon/payment-system/internal/config"
)

var (
	db     *pgxpool.Pool
	logger *zap.Logger
)

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	cfg, err := config.Read()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to load config: %v\n", err)
		return 1
	}

	logger, _ = zap.NewProduction()
	defer func() {
		_ = logger.Sync()
	}()

	pgxCfg, _ := pgx.ParseConfig("")
	pgxCfg.Host = cfg.Postgres.Connection.Host
	pgxCfg.Port = cfg.Postgres.Connection.Port
	pgxCfg.Database = cfg.Postgres.Database
	pgxCfg.User = cfg.Postgres.User
	pgxCfg.Password = cfg.Postgres.Password
	pgxCfg.Logger = zapadapter.NewLogger(logger)
	pgxCfg.LogLevel = pgx.LogLevelDebug
	pgxCfg.PreferSimpleProtocol = true

	pgxPoolCfg, _ := pgxpool.ParseConfig("")
	pgxPoolCfg.ConnConfig = pgxCfg
	pgxPoolCfg.MaxConns = cfg.Postgres.MaxConnections
	pgxPoolCfg.MinConns = cfg.Postgres.MinConnections

	db, err = pgxpool.ConnectConfig(context.Background(), pgxPoolCfg)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Unable to connect to database: %v\n", err))
		return 1
	}
	defer db.Close()

	return m.Run()
}
