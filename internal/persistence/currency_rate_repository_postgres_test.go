// +build integration

package persistence

import (
	"context"
	"fmt"
	"gitlab.com/zalleon/payment-system/pkg/money"
	"testing"
	"time"

	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"
	"gitlab.com/zalleon/payment-system/internal/domain"
)

func TestCurrencyRatePostgresRepository_PersistGetRemove(t *testing.T) {
	ctx := context.Background()

	rep := NewCurrencyRatePostgresRepository(db)

	now := time.Now()

	date := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	currency := money.CodeRUB
	rate, _ := decimal.NewFromString("1.23456789")

	currencyRate := &domain.CurrencyRate{
		Date:     date,
		Currency: currency,
		Rate:     rate,
	}

	if err := rep.Persist(ctx, currencyRate); err != nil {
		t.Fatal("Unable to persist currency rate")
	}

	if err := rep.Persist(ctx, currencyRate); err != nil {
		if err != domain.ErrEntityExists {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityExists))
	}

	currencyRateFromDB, err := rep.Get(ctx, currency, date)
	if err != nil {
		t.Fatal("Unable to get currency rate")
	}

	assert.Equal(t, currencyRateFromDB.Date, currencyRate.Date)
	assert.Equal(t, currencyRateFromDB.Currency, currencyRate.Currency)

	if !currencyRate.Rate.Equal(currencyRateFromDB.Rate) {
		t.Errorf("Expect %s got %s", currencyRate.Rate.String(), currencyRateFromDB.Rate.String())
	}

	if err := rep.Remove(ctx, currencyRate); err != nil {
		t.Fatal("Unable to remove currency rate")
	}

	_, err = rep.Get(ctx, currency, date)
	if err != nil {
		if err != domain.ErrEntityNotFound {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityNotFound))
	}
}
