// Package persistence contains implementation repositories
package persistence

import (
	"context"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/zalleon/payment-system/internal/domain"
)

type userPostgresRepository struct {
	table            string
	dbConnectionPool *pgxpool.Pool
}

func NewUserPostgresRepository(dbConnectionPool *pgxpool.Pool) *userPostgresRepository {
	return &userPostgresRepository{
		table:            "users",
		dbConnectionPool: dbConnectionPool,
	}
}

func (r *userPostgresRepository) Get(ctx context.Context, uuid uuid.UUID) (*domain.User, error) {
	user := &domain.User{
		ID: uuid,
	}
	if err := r.dbConnectionPool.QueryRow(
		ctx,
		fmt.Sprintf("SELECT name, email, country, city FROM %s WHERE id = $1", r.table),
		uuid.String(),
	).Scan(&user.Name, &user.Email, &user.Country, &user.City); err != nil {
		if err == pgx.ErrNoRows {
			return user, domain.ErrEntityNotFound
		}
		return user, err
	}
	return user, nil
}

func (r *userPostgresRepository) Remove(ctx context.Context, user *domain.User) error {
	if _, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("DELETE FROM %s WHERE id = $1", r.table),
		user.ID,
	); err != nil {
		return err
	}
	return nil
}

func (r *userPostgresRepository) Persist(ctx context.Context, user *domain.User) error {
	_, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("INSERT INTO %s (id,name,email,country,city) VALUES ($1,$2,$3,$4,$5)", r.table),
		user.ID.String(),
		user.Name,
		user.Email,
		user.Country,
		user.City,
	)

	if err != nil {
		if pgErr, ok := err.(interface{ SQLState() string }); ok {
			if pgErr.SQLState() == pgerrcode.UniqueViolation {
				return domain.ErrEntityExists
			}
		}
		return err
	}
	return nil
}
