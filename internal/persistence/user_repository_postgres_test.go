// +build integration

package persistence

import (
	"context"
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/magiconair/properties/assert"
	"gitlab.com/zalleon/payment-system/internal/domain"
)

func TestUserPostgresRepository_PersistGetRemove(t *testing.T) {
	ctx := context.Background()

	rep := NewUserPostgresRepository(db)

	id, _ := uuid.NewV4()

	user := &domain.User{
		ID:      id,
		Name:    "Alex",
		Email:   "alex@mail.com",
		Country: "Russia",
		City:    "Novosibirsk",
	}

	if err := rep.Persist(ctx, user); err != nil {
		t.Fatal("Unable to persist user")
	}

	if err := rep.Persist(ctx, user); err != nil {
		if err != domain.ErrEntityExists {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityExists))
	}

	userFromDB, err := rep.Get(ctx, id)
	if err != nil {
		t.Fatal("Unable to get user")
	}

	assert.Equal(t, userFromDB.Name, user.Name)
	assert.Equal(t, userFromDB.Email, user.Email)
	assert.Equal(t, userFromDB.Country, user.Country)
	assert.Equal(t, userFromDB.City, user.City)

	if err := rep.Remove(ctx, user); err != nil {
		t.Fatal("Unable to remove user")
	}

	_, err = rep.Get(ctx, id)
	if err != nil {
		if err != domain.ErrEntityNotFound {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityNotFound))
	}
}
