// +build integration

package persistence

import (
	"context"
	"fmt"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/pkg/money"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"

	"gitlab.com/zalleon/payment-system/internal/domain"
)

func TestTransactionPostgresRepository_PersistDuplicate(t *testing.T) {
	ctx := context.Background()

	transactionRepository := NewTransactionPostgresRepository(db)

	id, _ := uuid.NewV4()
	FromUserID, _ := uuid.NewV4()
	ToUserID, _ := uuid.NewV4()

	transaction := &domain.Transaction{
		ID:               id,
		Date:             time.Time{},
		Type:             "put",
		FromAccountID:    FromUserID,
		FromCurrency:     money.CodeRUB,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       decimal.NewFromInt(100),
		ToAccountID:      ToUserID,
		ToCurrency:       money.CodeRUB,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         decimal.NewFromInt(100),
	}

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}
	defer func() {
		if tx.IsCommit() {
			return
		}
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	if err := transactionRepository.Persist(ctx, transaction); err != nil {
		t.Fatalf("Got error while persist transaction. %v", err)
	}

	if err := transactionRepository.Persist(ctx, transaction); err != nil {
		if err != domain.ErrEntityExists {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityExists))
	}
}

func TestTransactionPostgresRepository_PersistGetRemove(t *testing.T) {
	ctx := context.Background()

	transactionRepository := NewTransactionPostgresRepository(db)

	id, _ := uuid.NewV4()
	FromUserID, _ := uuid.NewV4()
	ToUserID, _ := uuid.NewV4()

	transaction := &domain.Transaction{
		ID:               id,
		Date:             time.Time{},
		Type:             "put",
		FromAccountID:    FromUserID,
		FromCurrency:     money.CodeRUB,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       decimal.NewFromInt(100),
		ToAccountID:      ToUserID,
		ToCurrency:       money.CodeRUB,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         decimal.NewFromInt(100),
	}

	txManager := NewTransactionManagerPostgres(db)

	tx, err := txManager.Begin(ctx)
	if err != nil {
		t.Fatalf("Unable to begin transaction. %v", err)
	}
	defer func() {
		if tx.IsCommit() {
			return
		}
		if err = tx.Rollback(ctx); err != nil {
			t.Errorf("Got error while rollback transaction. %v", err)
		}
	}()

	ctx = usecase.WithTransaction(ctx, tx)

	if err := transactionRepository.Persist(ctx, transaction); err != nil {
		t.Fatalf("Unable to persist transaction. %v", err)
	}

	if err = tx.Commit(ctx); err != nil {
		t.Errorf("Got error while commit transaction. %v", err)
	}

	transactionLoaded, err := transactionRepository.Get(ctx, id)
	if err != nil {
		t.Fatal("Unable to get transaction")
	}

	assert.Equal(t, transactionLoaded.ID, transaction.ID)
	assert.Equal(t, transactionLoaded.Date, transaction.Date)
	assert.Equal(t, transactionLoaded.Type, transaction.Type)
	assert.Equal(t, transactionLoaded.FromAccountID, transaction.FromAccountID)
	assert.Equal(t, transactionLoaded.FromCurrency, transaction.FromCurrency)

	if !transaction.FromCurrencyRate.Equal(transactionLoaded.FromCurrencyRate) {
		t.Errorf("Expect %s got %s", transaction.FromCurrencyRate.String(), transactionLoaded.FromCurrencyRate.String())
	}

	if !transaction.FromAmount.Equal(transactionLoaded.FromAmount) {
		t.Errorf("Expect %s got %s", transaction.FromAmount.String(), transactionLoaded.FromAmount.String())
	}

	assert.Equal(t, transactionLoaded.ToAccountID, transaction.ToAccountID)
	assert.Equal(t, transactionLoaded.ToCurrency, transaction.ToCurrency)

	if !transaction.ToCurrencyRate.Equal(transactionLoaded.ToCurrencyRate) {
		t.Errorf("Expect %s got %s", transaction.ToCurrencyRate.String(), transactionLoaded.ToCurrencyRate.String())
	}

	if !transaction.ToAmount.Equal(transactionLoaded.ToAmount) {
		t.Errorf("Expect %s got %s", transaction.ToAmount.String(), transactionLoaded.ToAmount.String())
	}

	if err := transactionRepository.Remove(ctx, transaction); err != nil {
		t.Fatal("Unable to remove transaction")
	}

	_, err = transactionRepository.Get(ctx, id)
	if err != nil {
		if err != domain.ErrEntityNotFound {
			t.Fatal(fmt.Sprintf("Unexpected error: %v", err))
		}
	} else {
		t.Error(fmt.Sprintf("Expect error: %v", domain.ErrEntityNotFound))
	}
}
