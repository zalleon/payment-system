package persistence

import (
	"context"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type transactionPostgresRepository struct {
	table            string
	dbConnectionPool *pgxpool.Pool
}

func NewTransactionPostgresRepository(dbConnectionPool *pgxpool.Pool) *transactionPostgresRepository {
	return &transactionPostgresRepository{
		table:            "transactions",
		dbConnectionPool: dbConnectionPool,
	}
}

func (r *transactionPostgresRepository) Get(ctx context.Context, uuid uuid.UUID) (*domain.Transaction, error) {
	t := &domain.Transaction{
		ID: uuid,
	}

	if err := r.dbConnectionPool.QueryRow(
		ctx,
		fmt.Sprintf("SELECT date,type,from_account_id,from_currency,from_currency_rate,from_amount,to_account_id,to_currency,to_currency_rate,to_amount FROM %s WHERE id = $1", r.table),
		uuid,
	).Scan(&t.Date, &t.Type, &t.FromAccountID, &t.FromCurrency, &t.FromCurrencyRate, &t.FromAmount, &t.ToAccountID, &t.ToCurrency, &t.ToCurrencyRate, &t.ToAmount); err != nil {
		if err == pgx.ErrNoRows {
			return t, domain.ErrEntityNotFound
		}
		return t, err
	}
	return t, nil
}

func (r *transactionPostgresRepository) Persist(ctx context.Context, transaction *domain.Transaction) error {
	cnn, ok := ctx.Value(usecase.ContextTransactionKey{}).(*pgx.Conn)
	if !ok {
		return fmt.Errorf("unable to exec method without transaction")
	}

	_, err := cnn.Exec(
		ctx,
		fmt.Sprintf("INSERT INTO %s (id,date,type,from_account_id,from_currency,from_currency_rate,from_amount,to_account_id,to_currency,to_currency_rate,to_amount) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)", r.table),
		transaction.ID.String(),
		transaction.Date.Format("2006-01-02"),
		transaction.Type,
		transaction.FromAccountID.String(),
		transaction.FromCurrency,
		transaction.FromCurrencyRate,
		transaction.FromAmount,
		transaction.ToAccountID.String(),
		transaction.ToCurrency,
		transaction.ToCurrencyRate,
		transaction.ToAmount,
	)

	if err != nil {
		if pgErr, ok := err.(interface{ SQLState() string }); ok {
			if pgErr.SQLState() == pgerrcode.UniqueViolation {
				return domain.ErrEntityExists
			}
		}
		return err
	}
	return nil
}

func (r *transactionPostgresRepository) Remove(ctx context.Context, transaction *domain.Transaction) error {
	if _, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("DELETE FROM %s WHERE id = $1", r.table),
		transaction.ID,
	); err != nil {
		return err
	}
	return nil
}
