package persistence

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/zalleon/payment-system/internal/domain"
)

type currencyRatePostgresRepository struct {
	table            string
	dbConnectionPool *pgxpool.Pool
}

func NewCurrencyRatePostgresRepository(dbConnectionPool *pgxpool.Pool) *currencyRatePostgresRepository {
	return &currencyRatePostgresRepository{
		table:            "currency_rates",
		dbConnectionPool: dbConnectionPool,
	}
}

func (r *currencyRatePostgresRepository) Get(ctx context.Context, currency string, date time.Time) (*domain.CurrencyRate, error) {
	rate := &domain.CurrencyRate{}
	if err := r.dbConnectionPool.QueryRow(
		ctx,
		fmt.Sprintf("SELECT date, currency, rate FROM %s WHERE date = $1 AND currency=$2", r.table),
		date.Format("2006-01-02"),
		currency,
	).Scan(&rate.Date, &rate.Currency, &rate.Rate); err != nil {
		if err == pgx.ErrNoRows {
			return rate, domain.ErrEntityNotFound
		}
		return rate, err
	}
	return rate, nil
}

func (r *currencyRatePostgresRepository) Remove(ctx context.Context, rate *domain.CurrencyRate) error {
	if _, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("DELETE FROM %s WHERE date = $1 AND currency=$2", r.table),
		rate.Date.Format("2006-01-02"),
		rate.Currency,
	); err != nil {
		return err
	}
	return nil
}

func (r *currencyRatePostgresRepository) Persist(ctx context.Context, rate *domain.CurrencyRate) error {
	_, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("INSERT INTO %s (date, currency, rate) VALUES ($1,$2,$3)", r.table),
		rate.Date.Format("2006-01-02"),
		rate.Currency,
		rate.Rate,
	)

	if err != nil {
		if pgErr, ok := err.(interface{ SQLState() string }); ok {
			if pgErr.SQLState() == pgerrcode.UniqueViolation {
				return domain.ErrEntityExists
			}
		}
		return err
	}
	return nil
}
