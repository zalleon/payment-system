package persistence

import (
	"context"
	"fmt"

	"github.com/shopspring/decimal"
	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/gofrs/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/usecase"
)

type accountPostgresRepository struct {
	table            string
	dbConnectionPool *pgxpool.Pool
}

func NewAccountPostgresRepository(dbConnectionPool *pgxpool.Pool) *accountPostgresRepository {
	return &accountPostgresRepository{
		table:            "accounts",
		dbConnectionPool: dbConnectionPool,
	}
}

func (r *accountPostgresRepository) Get(ctx context.Context, uuid uuid.UUID) (*domain.Account, error) {
	cnn, ok := ctx.Value(usecase.ContextTransactionKey{}).(*pgx.Conn)
	if !ok {
		return nil, fmt.Errorf("unable to exec method without transaction")
	}

	account := &domain.Account{
		ID: uuid,
	}

	var currency string
	var balance decimal.Decimal

	if err := cnn.QueryRow(
		ctx,
		fmt.Sprintf("SELECT user_id, currency, balance FROM %s WHERE id = $1 FOR UPDATE", r.table),
		uuid.String(),
	).Scan(&account.User, &currency, &balance); err != nil {
		if err == pgx.ErrNoRows {
			return nil, domain.ErrEntityNotFound
		}
		return nil, err
	}

	curr, err := money.GetCurrency(currency)
	if err != nil {
		return nil, err
	}

	account.Balance = money.NewMoney(balance, curr)

	return account, nil
}

func (r *accountPostgresRepository) Remove(ctx context.Context, account *domain.Account) error {
	if _, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("DELETE FROM %s WHERE id = $1", r.table),
		account.ID.String(),
	); err != nil {
		return err
	}
	return nil
}

func (r *accountPostgresRepository) Persist(ctx context.Context, account *domain.Account) error {
	_, err := r.dbConnectionPool.Exec(
		ctx,
		fmt.Sprintf("INSERT INTO %s (id,user_id,currency,balance) VALUES ($1,$2,$3,$4)", r.table),
		account.ID.String(),
		account.User.String(),
		account.Balance.Currency().Code,
		account.Balance.Amount(),
	)

	if err != nil {
		if pgErr, ok := err.(interface{ SQLState() string }); ok {
			if pgErr.SQLState() == pgerrcode.UniqueViolation {
				return domain.ErrEntityExists
			}
		}
		return err
	}
	return nil
}

func (r *accountPostgresRepository) UpdateBalance(ctx context.Context, account *domain.Account) error {
	cnn, ok := ctx.Value(usecase.ContextTransactionKey{}).(*pgx.Conn)
	if !ok {
		return fmt.Errorf("unable to exec method without transaction")
	}

	if tag, err := cnn.Exec(
		ctx,
		fmt.Sprintf("UPDATE %s SET balance=$1 WHERE id = $2", r.table),
		account.Balance.Amount(),
		account.ID.String(),
	); err != nil {
		return err
	} else if tag.RowsAffected() == 0 {
		return domain.ErrEntityNotUpdated
	}
	return nil
}
