package usecase_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"

	"github.com/stretchr/testify/mock"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/pkg/money"
)

func TestNewPutMoneyCommand_WrongAccount(t *testing.T) {
	if _, err := usecase.NewPutMoneyCommand("", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "account cannot be blank", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := usecase.NewPutMoneyCommand("some-wrong-uuid", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "account must be a valid UUID v4", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewPutMoneyCommand_WrongAmount(t *testing.T) {
	id, _ := uuid.NewV4()
	if _, err := usecase.NewPutMoneyCommand(id.String(), decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "amount should be greater than 0", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewPutMoneyCommand_Success(t *testing.T) {
	id, _ := uuid.NewV4()
	amount := decimal.NewFromInt(100)

	cmd, cmdErr := usecase.NewPutMoneyCommand(id.String(), amount)
	if cmdErr != nil {
		t.Fatal("Expect validation error")
	}

	assert.Equal(t, id, cmd.AccountID)
	assert.Equal(t, true, cmd.Amount.Equal(amount))
}

func TestHandle_Success(t *testing.T) {
	accountID, _ := uuid.NewV4()
	userID, _ := uuid.NewV4()
	curr, _ := money.GetCurrency(money.CodeRUB)

	accountRep := &mocks.AccountRepositoryMock{}
	accountRep.On(
		"Get",
		mock.Anything,
		mock.Anything,
	).Return(&domain.Account{
		ID:      accountID,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), curr),
	}, nil)
	accountRep.On(
		"UpdateBalance",
		mock.Anything,
		mock.Anything,
	).Return(nil)

	cmd, err := usecase.NewPutMoneyCommand(accountID.String(), decimal.NewFromInt(100))
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}

	transactionRepository := &mocks.TransactionRepositoryMock{}
	transactionRepository.On(
		"Persist",
		mock.Anything,
		mock.Anything,
	).Return(func(ctx context.Context, t *domain.Transaction) error {
		if t.Type == "put" && t.FromAccountID == accountID && t.FromCurrency == curr.Code && t.FromCurrencyRate.Equal(decimal.NewFromInt(1)) && t.FromAmount.Equal(cmd.Amount) &&
			t.ToAccountID == accountID && t.ToCurrency == curr.Code && t.ToCurrencyRate.Equal(decimal.NewFromInt(1)) && t.ToAmount.Equal(cmd.Amount) {
			return nil
		}
		return fmt.Errorf("wrong transaction payload")
	})

	tx := &mocks.TxMock{}
	tx.On("Conn").Return(nil)
	tx.On("IsCommit").Return(true)
	tx.On("Commit", mock.Anything).Return(nil)
	tx.On("Rollback", mock.Anything).Return(nil)

	txManager := &mocks.TxManagerMock{}
	txManager.On("Begin", mock.Anything).Return(tx, nil)

	handler := usecase.NewPutMoneyCommandHandler(
		nil,
		accountRep,
		transactionRepository,
		txManager,
	)

	if err := handler.Handle(context.Background(), cmd); err != nil {
		t.Errorf("Unexpected error %v", err)
	}
}
