package usecase

import (
	"testing"

	"github.com/gofrs/uuid"
	"github.com/magiconair/properties/assert"

	"gitlab.com/zalleon/payment-system/pkg/money"
)

func TestNewAddAccountCommand_WrongUser(t *testing.T) {
	if _, err := NewAddAccountCommand("", ""); err != nil {
		assert.Equal(t, "user cannot be blank", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddAccountCommand("1", ""); err != nil {
		assert.Equal(t, "user must be a valid UUID v4", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewAddAccountCommand_WrongCurrency(t *testing.T) {
	id, _ := uuid.NewV4()

	if _, err := NewAddAccountCommand(id.String(), ""); err != nil {
		assert.Equal(t, "currency cannot be blank", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddAccountCommand(id.String(), "RUBS"); err != nil {
		assert.Equal(t, "currency must be in a valid format", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddAccountCommand(id.String(), "123"); err != nil {
		assert.Equal(t, "currency must be in a valid format", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewAddAccountCommand_Success(t *testing.T) {
	id, _ := uuid.NewV4()
	currency := money.CodeRUB

	cmd, cmdErr := NewAddAccountCommand(id.String(), currency)
	if cmdErr != nil {
		t.Fatal("Unable to create command")
	}

	assert.Equal(t, id, cmd.User)
	assert.Equal(t, currency, cmd.Currency)
}
