package usecase_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/persistence/mocks"
	sMocks "gitlab.com/zalleon/payment-system/internal/services/mocks"

	"github.com/gofrs/uuid"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/zalleon/payment-system/internal/usecase"
	"gitlab.com/zalleon/payment-system/pkg/money"
)

func TestNewTransferMoneyCommand_Success(t *testing.T) {
	fromID, _ := uuid.NewV4()
	toID, _ := uuid.NewV4()

	currencyEUR, _ := money.GetCurrency(money.CodeEUR)
	amount := money.NewMoney(decimal.NewFromInt(100), currencyEUR)

	cmd, initCommandErr := usecase.NewTransferMoneyCommand(fromID.String(), toID.String(), amount)
	if initCommandErr != nil {
		t.Fatalf("Unexpected error %+v", initCommandErr)
	}

	assert.Equal(t, fromID, cmd.FromAccountID)
	assert.Equal(t, toID, cmd.ToAccountID)

	isEqual, compareErr := cmd.Money.Equals(amount)
	if compareErr != nil {
		t.Fatalf("Unexpected error %+v", initCommandErr)
	}
	assert.Equal(t, true, isEqual)
}

func TestNewTransferMoneyCommand_ValidationError(t *testing.T) {
	fromID, _ := uuid.NewV4()
	toID, _ := uuid.NewV4()
	currencyEUR, _ := money.GetCurrency(money.CodeEUR)

	tcs := []struct {
		fromAccount   string
		toAccount     string
		amount        *money.Money
		expectedError string
	}{
		{"", "", nil, "fromAccountID cannot be blank"},
		{"wrong-uuid", "", nil, "fromAccountID must be a valid UUID v4"},
		{fromID.String(), "", nil, "toAccountID cannot be blank"},
		{fromID.String(), "wrong-uuid", nil, "toAccountID must be a valid UUID v4"},
		{fromID.String(), toID.String(), money.NewMoney(decimal.NewFromInt(0), currencyEUR), "money should be greater than 0"},
		{fromID.String(), toID.String(), money.NewMoney(decimal.NewFromInt(-1), currencyEUR), "money should be greater than 0"},
	}

	for _, tc := range tcs {
		if _, err := usecase.NewTransferMoneyCommand(tc.fromAccount, tc.toAccount, tc.amount); err != nil {
			assert.Equal(t, tc.expectedError, err.Error())
		} else {
			t.Error("Expect validation error, actual nil")
		}
	}
}

/*
	 U+ 1. FromAccountID == ToAccountID -> ValidationError
	 I+	2. tx.Begin() -> Error
	 I+	3. tx.Rollback() -> Error
	 U+	4. Get(FromAccount) -> ValidationError
	 U+	5. Get(FromAccount) -> Error
	 U+ 6. Get(ToAccountID) -> ValidationError
	 U+	7. Get(ToAccountID) -> Error
	 U+	8. CommandCurrency != FromAccount.Currency && CommandCurrency != ToAccount.Currency -> ValidationError
	 I+	9. Transaction.Persist() -> Error
	 I+ 10. tx.Commit(ctx) -> error

		transferMoneyWidthSameCurrency()
		1. Balance.LessThan(cmd.Money) (Diff currencies) -> Error
		2. Balance.LessThan(cmd.Money) -> ValidationError
		3. Balance.Subtract(cmd.Money) (Diff currencies) -> Error
		4. UpdateBalance(sourceAccount) -> Error
		5. Balance.Add(cmd.Money) (Diff currencies) -> Error
		6. UpdateBalance(targetAccount) -> Error

		transferMoneyBasedOnSourceAccountCurrency()
		1. Balance.LessThan(cmd.Money)(Diff currencies) -> Error
	    2. Balance.LessThan(cmd.Money) -> ValidationError
		3. Balance.Subtract(cmd.Money)(Diff currencies) -> Error
		4. UpdateBalance(sourceAccount) -> Error
		5. Convert() -> Error
		6. Balance.Add(convertedMoney)(Diff currencies) -> Error
		7. UpdateBalance(targetAccount) -> Error

		transferMoneyBasedOnTargetAccountCurrency()
		1. Convert() -> Error
		2. Balance.LessThan(convertedMoney)(Diff currencies) -> Error
	    3. Balance.LessThan(convertedMoney) -> Error
		4. Balance.Subtract(convertedMoney)(Diff currencies) -> Error
		5. UpdateBalance(sourceAccount) -> Error
		6. Balance.Add(cmd.Money)(Diff currencies) -> Error
		7. UpdateBalance(targetAccount) -> Error
*/
func TestHandle_TransferWidthSameCurrency_Success(t *testing.T) {
	currencyUSD, _ := money.GetCurrency(money.CodeUSD)

	userID, _ := uuid.NewV4()
	fromAccountID, _ := uuid.NewV4()
	fromAccount := &domain.Account{
		ID:      fromAccountID,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), currencyUSD),
	}

	toAccountID, _ := uuid.NewV4()
	toAccount := &domain.Account{
		ID:      toAccountID,
		User:    userID,
		Balance: money.NewMoney(decimal.NewFromInt(100), currencyUSD),
	}
	expectAccountBalance := money.NewMoney(decimal.NewFromInt(200), currencyUSD)
	transferMoney := money.NewMoney(decimal.NewFromInt(100), currencyUSD)

	cmd, err := usecase.NewTransferMoneyCommand(fromAccountID.String(), toAccountID.String(), transferMoney)
	if err != nil {
		t.Errorf("Unexpected error %v", err)
	}

	accountRep := &mocks.AccountRepositoryMock{}
	accountRep.On("Get", mock.Anything, mock.Anything).Return(
		func(ctx context.Context, ID uuid.UUID) *domain.Account {
			switch ID {
			case fromAccountID:
				return fromAccount
			case toAccountID:
				return toAccount
			}
			return nil
		}, nil,
	)
	accountRep.On("UpdateBalance", mock.Anything, mock.Anything).Return(
		func(atx context.Context, acc *domain.Account) error {
			switch acc.ID {
			case fromAccountID:
				if !acc.Balance.IsZero() {
					return fmt.Errorf("expect zero balance. actual %v", acc.Balance.Amount())
				}
				return nil
			case toAccountID:
				isEqual, err := acc.Balance.Equals(expectAccountBalance)
				if err != nil {
					return err
				}
				if !isEqual {
					return fmt.Errorf("expect %v balance. actual %v", expectAccountBalance.Amount(), acc.Balance.Amount())
				}
				return nil
			}
			return fmt.Errorf("unable to update balance. account doesn't exist")
		})

	transactionRepository := &mocks.TransactionRepositoryMock{}
	transactionRepository.On("Persist", mock.Anything, mock.Anything).Return(
		func(ctx context.Context, t *domain.Transaction) error {
			if t.FromCurrency != fromAccount.Balance.Currency().Code ||
				!t.FromAmount.Equal(transferMoney.Amount()) ||
				t.ToCurrency != toAccount.Balance.Currency().Code ||
				!t.ToAmount.Equal(transferMoney.Amount()) {
				return fmt.Errorf("wrong transaction payload. %+v", t)
			}
			return nil
		},
	)

	handler := usecase.NewTransferMoneyCommandHandler(
		nil,
		&sMocks.CurrencyConverterMock{},
		accountRep,
		transactionRepository,
		MustTxManager(),
	)

	if err := handler.Handle(context.Background(), cmd); err != nil {
		t.Errorf("Unexpected error %v", err)
	}
}

func MustTxManager() *mocks.TxManagerMock {
	tx := &mocks.TxMock{}
	tx.On("Conn").Return(nil)
	tx.On("IsCommit").Return(true)
	tx.On("Commit", mock.Anything).Return(nil)
	tx.On("Rollback", mock.Anything).Return(nil)

	txManager := &mocks.TxManagerMock{}
	txManager.On("Begin", mock.Anything).Return(tx, nil)
	return txManager
}

func TestHandle_TransferBasedOnSourceAccountCurrency_Success(t *testing.T) {

}

func TestHandle_TransferBasedOnTargetAccountCurrency_success(t *testing.T) {

}
