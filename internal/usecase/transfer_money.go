package usecase

import (
	"context"
	"time"

	"go.uber.org/zap"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/zalleon/payment-system/internal/domain"
	"gitlab.com/zalleon/payment-system/internal/services"
	"gitlab.com/zalleon/payment-system/pkg/money"
)

type transferMoneyCommand struct {
	FromAccountID uuid.UUID
	ToAccountID   uuid.UUID
	Money         *money.Money
}

func NewTransferMoneyCommand(fromAccountID, toAccountID string, money *money.Money) (*transferMoneyCommand, *domain.ValidationError) {
	if err := validation.Validate(
		fromAccountID,
		validation.Required,
		is.UUIDv4,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "fromAccountID",
			Message: err.Error(),
		}
	}

	fromID, err := uuid.FromString(fromAccountID)
	if err != nil {
		return nil, &domain.ValidationError{
			Field:   "fromAccountID",
			Message: err.Error(),
		}
	}

	if err = validation.Validate(
		toAccountID,
		validation.Required,
		is.UUIDv4,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "toAccountID",
			Message: err.Error(),
		}
	}

	toID, err := uuid.FromString(toAccountID)
	if err != nil {
		return nil, &domain.ValidationError{
			Field:   "fromAccountID",
			Message: err.Error(),
		}
	}

	if money.IsNegative() || money.IsZero() {
		return nil, &domain.ValidationError{
			Field:   "money",
			Message: "should be greater than 0",
		}
	}

	return &transferMoneyCommand{
		FromAccountID: fromID,
		ToAccountID:   toID,
		Money:         money,
	}, nil
}

type TransferMoneyCommandHandler struct {
	logger                *zap.Logger
	txManager             TxManagerInterface
	currencyConverter     services.CurrencyConverterInterface
	accountRepository     domain.AccountRepositoryInterface
	transactionRepository domain.TransactionRepositoryInterface
}

func NewTransferMoneyCommandHandler(
	logger *zap.Logger,
	currencyConverter services.CurrencyConverterInterface,
	accountRepository domain.AccountRepositoryInterface,
	transactionRepository domain.TransactionRepositoryInterface,
	txManager TxManagerInterface,
) *TransferMoneyCommandHandler {
	return &TransferMoneyCommandHandler{
		logger:                logger,
		currencyConverter:     currencyConverter,
		accountRepository:     accountRepository,
		transactionRepository: transactionRepository,
		txManager:             txManager,
	}
}

// Handle Transfer money between accounts
func (h *TransferMoneyCommandHandler) Handle(ctx context.Context, cmd *transferMoneyCommand) error {
	if cmd.FromAccountID == cmd.ToAccountID {
		return &domain.ValidationError{
			Field:   "from",
			Message: "from account and to account mut be different",
		}
	}

	tx, err := h.txManager.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to begin transfer money transaction.")
	}

	// Put transaction into context
	ctx = WithTransaction(ctx, tx)
	defer func(ctx context.Context, tx TxInterface) {
		// Rollback transaction if not committed
		if tx.IsCommit() {
			return
		}
		if errRollback := tx.Rollback(ctx); errRollback != nil {
			h.logger.Error(errRollback.Error())
		}
	}(ctx, tx)

	sourceAccount, err := h.accountRepository.Get(ctx, cmd.FromAccountID)
	if err != nil {
		if err == domain.ErrEntityNotFound {
			return &domain.ValidationError{
				Field:   "accountFrom",
				Message: "does not exist",
			}
		}
		return errors.Wrapf(err, "unable to get for update account %s.", cmd.FromAccountID.String())
	}

	targetAccount, err := h.accountRepository.Get(ctx, cmd.ToAccountID)
	if err != nil {
		if err == domain.ErrEntityNotFound {
			return &domain.ValidationError{
				Field:   "accountTo",
				Message: "does not exist",
			}
		}
		return errors.Wrapf(err, "unable to get for update account %s.", cmd.FromAccountID.String())
	}

	if !cmd.Money.Currency().Equal(sourceAccount.Balance.Currency()) && !cmd.Money.Currency().Equal(targetAccount.Balance.Currency()) {
		return &domain.ValidationError{
			Field:   "currency",
			Message: "currency must belong to one of the accounts currency",
		}
	}

	var transaction *domain.Transaction

	// Transfer money cases
	switch {
	case sourceAccount.Balance.Currency().Equal(targetAccount.Balance.Currency()):
		t, errTransfer := h.transferMoneyWidthSameCurrency(ctx, sourceAccount, targetAccount, cmd)
		if errTransfer != nil {
			return errTransfer
		}
		transaction = t
	case sourceAccount.Balance.Currency().Equal(cmd.Money.Currency()):
		t, errTransfer := h.transferMoneyBasedOnSourceAccountCurrency(ctx, sourceAccount, targetAccount, cmd)
		if errTransfer != nil {
			return errTransfer
		}
		transaction = t
	case targetAccount.Balance.Currency().Equal(cmd.Money.Currency()):
		t, errTransfer := h.transferMoneyBasedOnTargetAccountCurrency(ctx, sourceAccount, targetAccount, cmd)
		if errTransfer != nil {
			return errTransfer
		}
		transaction = t
	default:
		return errors.New("Wrong condition")
	}

	err = h.transactionRepository.Persist(ctx, transaction)
	if err != nil {
		return errors.Wrap(err, "unable to create transaction")
	}

	// Commit
	if err := tx.Commit(ctx); err != nil {
		return errors.Wrap(err, "unable to commit put money transaction.")
	}
	return nil
}

func (h *TransferMoneyCommandHandler) transferMoneyWidthSameCurrency(ctx context.Context, sourceAccount, targetAccount *domain.Account, cmd *transferMoneyCommand) (*domain.Transaction, error) {
	isLess, err := sourceAccount.Balance.LessThan(cmd.Money)
	if err != nil {
		return nil, err
	}
	if isLess {
		return nil, &domain.ValidationError{
			Field:   "currency",
			Message: "Insufficient funds in the account",
		}
	}

	// Update balance
	sourceAccount.Balance, err = sourceAccount.Balance.Subtract(cmd.Money)
	if err != nil {
		return nil, err
	}

	// Persist source account balance
	if errUpdateSourceAccountBalance := h.accountRepository.UpdateBalance(ctx, sourceAccount); errUpdateSourceAccountBalance != nil {
		return nil, errors.Wrap(errUpdateSourceAccountBalance, "unable to update account balance")
	}

	// Update balance
	targetAccount.Balance, err = targetAccount.Balance.Add(cmd.Money)
	if err != nil {
		return nil, err
	}

	// Persist target account balance
	if err = h.accountRepository.UpdateBalance(ctx, targetAccount); err != nil {
		return nil, errors.Wrap(err, "unable to update account balance")
	}

	tID, err := uuid.NewV4()
	if err != nil {
		return nil, errors.Wrap(err, "unable to create transaction ID")
	}

	// Init transaction
	t := &domain.Transaction{
		ID:               tID,
		Date:             time.Now(),
		Type:             "transfer",
		FromAccountID:    cmd.FromAccountID,
		FromCurrency:     cmd.Money.Currency().Code,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       cmd.Money.Amount(),
		ToAccountID:      cmd.ToAccountID,
		ToCurrency:       cmd.Money.Currency().Code,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         cmd.Money.Amount(),
	}
	return t, nil
}

func (h *TransferMoneyCommandHandler) transferMoneyBasedOnSourceAccountCurrency(ctx context.Context, sourceAccount, targetAccount *domain.Account, cmd *transferMoneyCommand) (*domain.Transaction, error) {
	isLess, err := sourceAccount.Balance.LessThan(cmd.Money)
	if err != nil {
		return nil, err
	}
	if isLess {
		return nil, &domain.ValidationError{
			Field:   "currency",
			Message: "Insufficient funds in the account",
		}
	}

	// Update balance
	sourceAccount.Balance, err = sourceAccount.Balance.Subtract(cmd.Money)
	if err != nil {
		return nil, err
	}

	// Persist source account balance
	if errUpdateSourceAccountBalance := h.accountRepository.UpdateBalance(ctx, sourceAccount); errUpdateSourceAccountBalance != nil {
		return nil, errors.Wrap(errUpdateSourceAccountBalance, "unable to update account balance")
	}

	// Convert money
	convertedMoney, err := h.currencyConverter.Convert(time.Now(), cmd.Money, targetAccount.Balance.Currency())
	if err != nil {
		return nil, err
	}

	// Update balance
	targetAccount.Balance, err = targetAccount.Balance.Add(convertedMoney)
	if err != nil {
		return nil, err
	}

	// Persist target account balance
	err = h.accountRepository.UpdateBalance(ctx, targetAccount)
	if err != nil {
		return nil, errors.Wrap(err, "unable to update account balance")
	}

	tID, err := uuid.NewV4()
	if err != nil {
		return nil, errors.Wrap(err, "unable to create transaction ID")
	}

	// Init transaction
	t := &domain.Transaction{
		ID:               tID,
		Date:             time.Now(),
		Type:             "transfer",
		FromAccountID:    cmd.FromAccountID,
		FromCurrency:     cmd.Money.Currency().Code,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       cmd.Money.Amount(),
		ToAccountID:      cmd.ToAccountID,
		ToCurrency:       targetAccount.Balance.Currency().Code,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         convertedMoney.Amount(),
	}
	return t, nil
}

func (h *TransferMoneyCommandHandler) transferMoneyBasedOnTargetAccountCurrency(ctx context.Context, sourceAccount, targetAccount *domain.Account, cmd *transferMoneyCommand) (*domain.Transaction, error) {
	// Convert money
	convertedMoney, err := h.currencyConverter.Convert(time.Now(), cmd.Money, sourceAccount.Balance.Currency())
	if err != nil {
		return nil, err
	}

	isLess, err := sourceAccount.Balance.LessThan(convertedMoney)
	if err != nil {
		return nil, err
	}

	if isLess {
		return nil, &domain.ValidationError{
			Field:   "currency",
			Message: "Insufficient funds in the account",
		}
	}

	// Update balance
	sourceAccount.Balance, err = sourceAccount.Balance.Subtract(convertedMoney)
	if err != nil {
		return nil, err
	}

	// Persist source account balance
	if errUpdateSourceAccountBalance := h.accountRepository.UpdateBalance(ctx, sourceAccount); errUpdateSourceAccountBalance != nil {
		return nil, errors.Wrap(errUpdateSourceAccountBalance, "unable to update account balance")
	}

	// Update balance
	targetAccount.Balance, err = targetAccount.Balance.Add(cmd.Money)
	if err != nil {
		return nil, err
	}

	// Persist target account balance
	err = h.accountRepository.UpdateBalance(ctx, targetAccount)
	if err != nil {
		return nil, errors.Wrap(err, "unable to update account balance")
	}

	tID, err := uuid.NewV4()
	if err != nil {
		return nil, errors.Wrap(err, "unable to create transaction ID")
	}

	// Init transaction
	t := &domain.Transaction{
		ID:               tID,
		Date:             time.Now(),
		Type:             "transfer",
		FromAccountID:    cmd.FromAccountID,
		FromCurrency:     convertedMoney.Currency().Code,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       convertedMoney.Amount(),
		ToAccountID:      cmd.ToAccountID,
		ToCurrency:       cmd.Money.Currency().Code,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         cmd.Money.Amount(),
	}
	return t, nil
}
