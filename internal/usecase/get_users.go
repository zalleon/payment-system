package usecase

import (
	"context"
	"fmt"
	"net/url"
	"regexp"

	rqp "github.com/timsolov/rest-query-parser"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

// UserDto represents the user
//
// A user is the security principal for this application.
//
// A user can have accounts.
//
// swagger:model User
type UserDto struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Country string `json:"country"`
	City    string `json:"city"`
}

type getUsersQuery struct {
	Query string
	Args  []interface{}
}

func NewGetUsersQuery(requestURI string) (*getUsersQuery, error) {
	parsedRequestURI, err := url.Parse(requestURI)
	if err != nil {
		return nil, err
	}
	q, err := rqp.NewParse(parsedRequestURI.Query(), rqp.Validations{
		"limit:required": rqp.Max(20),
		"sort":           rqp.In("name", "country", "city"),
		"name":           nil,
		"country":        nil,
		"city":           nil,
	})
	if err != nil {
		return nil, err
	}

	q.Fields = []string{"id", "name", "country", "city"}

	i := 0
	re := regexp.MustCompile(`\?`)
	return &getUsersQuery{
		// TODO: fix lib for PostageSQL dialect. This is hack for replace ? -> $1
		Query: re.ReplaceAllStringFunc(q.SQL("users"), func(s string) string { i++; return fmt.Sprintf("$%d", i) }),
		Args:  q.Args(),
	}, nil
}

type GetUsersQueryHandler struct {
	db *pgxpool.Pool
}

func NewGetUsersQueryHandler(db *pgxpool.Pool) *GetUsersQueryHandler {
	return &GetUsersQueryHandler{db: db}
}

func (h *GetUsersQueryHandler) Handle(ctx context.Context, query *getUsersQuery) ([]UserDto, error) {
	rows, err := h.db.Query(ctx, query.Query, query.Args...)
	if err != nil {
		return nil, errors.Wrap(err, "unable to execute get users query")
	}
	defer rows.Close()

	users := make([]UserDto, 0)

	for rows.Next() {
		var user UserDto
		err = rows.Scan(&user.ID, &user.Name, &user.Country, &user.City)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	if rows.Err() != nil {
		return nil, err
	}

	return users, nil
}
