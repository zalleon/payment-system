// Package usecase contains application business logic
package usecase

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/gofrs/uuid"
	"gitlab.com/zalleon/payment-system/internal/domain"
)

type registerUserCommand struct {
	Name    string
	Email   string
	Country string
	City    string
}

func NewRegisterUserCommand(name string, email string, country string, city string) (*registerUserCommand, *domain.ValidationError) {
	if err := validation.Validate(
		name,
		validation.Required,
		validation.Length(5, 150),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "name",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		email,
		validation.Required,
		is.Email,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "email",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		country,
		validation.Required,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "country",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		city,
		validation.Required,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "city",
			Message: err.Error(),
		}
	}

	return &registerUserCommand{
		Name:    name,
		Email:   email,
		Country: country,
		City:    city,
	}, nil
}

type RegisterUserCommandHandler struct {
	userRepository domain.UserRepositoryInterface
}

func NewRegisterUserCommandHandler(userRepository domain.UserRepositoryInterface) *RegisterUserCommandHandler {
	return &RegisterUserCommandHandler{userRepository: userRepository}
}

func (h *RegisterUserCommandHandler) Handle(ctx context.Context, cmd *registerUserCommand) error {
	id, _ := uuid.NewV4()
	user := &domain.User{
		ID:      id,
		Name:    cmd.Name,
		Email:   cmd.Email,
		Country: cmd.Country,
		City:    cmd.City,
	}

	if err := h.userRepository.Persist(ctx, user); err != nil {
		if err == domain.ErrEntityExists {
			return &domain.ValidationError{
				Field:   "user",
				Message: domain.ErrEntityExists.Error(),
			}
		}
		return err
	}
	return nil
}
