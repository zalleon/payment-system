package usecase

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/magiconair/properties/assert"
	"github.com/shopspring/decimal"
)

func TestNewAddCurrencyRateCommand_WrongDate(t *testing.T) {
	if _, err := NewAddCurrencyRateCommand("", "", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "date cannot be blank", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddCurrencyRateCommand("20000101", "", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "date must be a valid date", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	now := time.Now()
	date := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	date = date.Add(24 * time.Hour)
	date = date.Add(1 * time.Second)

	if _, err := NewAddCurrencyRateCommand(date.Format("2006-01-02"), "", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "date must be less or equal today UTC midnight", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewAddCurrencyRateCommand_WrongCurrency(t *testing.T) {
	if _, err := NewAddCurrencyRateCommand("2006-01-01", "", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "currency cannot be blank", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddCurrencyRateCommand("2006-01-01", "RUBS", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "currency must be in a valid format", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddCurrencyRateCommand("2006-01-01", "123", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "currency must be in a valid format", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewAddCurrencyRateCommand_WrongRate(t *testing.T) {
	if _, err := NewAddCurrencyRateCommand("2006-01-01", "RUB", decimal.NewFromInt(0)); err != nil {
		assert.Equal(t, "rate should be greater than 0", err.Error())
	} else {
		t.Error("Expect validation error")
	}

	if _, err := NewAddCurrencyRateCommand("2006-01-01", "RUB", decimal.NewFromInt(-1)); err != nil {
		assert.Equal(t, "rate should be greater than 0", err.Error())
	} else {
		t.Error("Expect validation error")
	}
}

func TestNewAddCurrencyRateCommand_Success(t *testing.T) {
	date, err := time.Parse("2006-01-02", "2020-01-01")
	if err != nil {
		t.Fatal("Invalid date")
	}
	currency := money.CodeRUB
	rate, err := decimal.NewFromString("1.123456")
	if err != nil {
		t.Fatal("Invalid rate")
	}

	cmd, cmdErr := NewAddCurrencyRateCommand(date.Format("2006-01-02"), currency, rate)
	if cmdErr != nil {
		t.Fatal(fmt.Sprintf("Unable to create command. %+v", err))
	}

	assert.Equal(t, date, cmd.Date)
	assert.Equal(t, currency, cmd.Currency)
	assert.Equal(t, rate, cmd.Rate)
}
