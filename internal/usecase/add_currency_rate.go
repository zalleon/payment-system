package usecase

import (
	"context"
	"fmt"
	"regexp"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/shopspring/decimal"

	"gitlab.com/zalleon/payment-system/internal/domain"
)

type addCurrencyRateCommand struct {
	Date     time.Time
	Currency string
	Rate     decimal.Decimal
}

func NewAddCurrencyRateCommand(date string, currency string, rate decimal.Decimal) (*addCurrencyRateCommand, *domain.ValidationError) {
	if err := validation.Validate(
		date,
		validation.Required,
		validation.Date("2006-01-02"),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "date",
			Message: err.Error(),
		}
	}

	parsedDate, _ := time.Parse("2006-01-02", date)
	now := time.Now()
	if err := validation.Validate(parsedDate, validation.Max(time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC))); err != nil {
		return nil, &domain.ValidationError{
			Field:   "date",
			Message: "must be less or equal today UTC midnight",
		}
	}

	if err := validation.Validate(
		currency,
		validation.Required,
		validation.Match(regexp.MustCompile("^[A-Z]{3}$")),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "currency",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		rate,
		validation.Required,
		validation.By(func(value interface{}) error {
			if v, ok := value.(decimal.Decimal); !ok || v.LessThanOrEqual(decimal.NewFromInt(0)) {
				return fmt.Errorf("should be greater than 0")
			}
			return nil
		}),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "rate",
			Message: err.Error(),
		}
	}

	cmd := &addCurrencyRateCommand{
		Date:     parsedDate,
		Currency: currency,
		Rate:     rate,
	}

	return cmd, nil
}

type AddCurrencyRateCommandHandler struct {
	currencyRateRepository domain.CurrencyRateRepositoryInterface
}

func NewAddCurrencyRateCommandHandler(currencyRateRepo domain.CurrencyRateRepositoryInterface) *AddCurrencyRateCommandHandler {
	return &AddCurrencyRateCommandHandler{
		currencyRateRepository: currencyRateRepo,
	}
}

func (h *AddCurrencyRateCommandHandler) Handle(ctx context.Context, cmd *addCurrencyRateCommand) error {
	rate := &domain.CurrencyRate{
		Date:     cmd.Date,
		Currency: cmd.Currency,
		Rate:     cmd.Rate,
	}

	if err := h.currencyRateRepository.Persist(ctx, rate); err != nil {
		if err == domain.ErrEntityExists {
			return &domain.ValidationError{
				Field:   "currency rate",
				Message: domain.ErrEntityExists.Error(),
			}
		}
		return err
	}
	return nil
}
