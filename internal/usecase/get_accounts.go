package usecase

import (
	"context"
	"fmt"
	"net/url"
	"regexp"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	rqp "github.com/timsolov/rest-query-parser"
)

// AccountDto represents the account for this application
//
// swagger:model
type AccountDto struct {
	ID       string          `json:"id"`
	UserID   string          `json:"user_id"`
	Currency string          `json:"currency"`
	Balance  decimal.Decimal `json:"balance"`
}

type getAccountsQuery struct {
	Query string
	Args  []interface{}
}

func NewGetAccountsQuery(requestURI string) (*getAccountsQuery, error) {
	parsedRequestURI, err := url.Parse(requestURI)
	if err != nil {
		return nil, err
	}
	q, err := rqp.NewParse(parsedRequestURI.Query(), rqp.Validations{
		"limit:required": rqp.Max(20),
		"sort":           rqp.In("user_id", "currency", "balance"),
		"user_id":        nil,
		"currency":       nil,
	})
	if err != nil {
		return nil, err
	}

	q.Fields = []string{"id", "user_id", "currency", "balance"}

	i := 0
	re := regexp.MustCompile(`\?`)
	return &getAccountsQuery{
		// TODO: fix lib for PostageSQL dialect. This is hack for replace ? -> $1
		Query: re.ReplaceAllStringFunc(q.SQL("accounts"), func(s string) string { i++; return fmt.Sprintf("$%d", i) }),
		Args:  q.Args(),
	}, nil
}

type GetAccountsQueryHandler struct {
	db *pgxpool.Pool
}

func NewGetAccountsQueryHandler(db *pgxpool.Pool) *GetAccountsQueryHandler {
	return &GetAccountsQueryHandler{db: db}
}

func (h *GetAccountsQueryHandler) Handle(ctx context.Context, query *getAccountsQuery) ([]AccountDto, error) {
	rows, err := h.db.Query(ctx, query.Query, query.Args...)
	if err != nil {
		return nil, errors.Wrap(err, "unable to execute get accounts query")
	}
	defer rows.Close()

	accounts := make([]AccountDto, 0)

	for rows.Next() {
		var account AccountDto
		err = rows.Scan(&account.ID, &account.UserID, &account.Currency, &account.Balance)
		if err != nil {
			return nil, err
		}
		accounts = append(accounts, account)
	}

	if rows.Err() != nil {
		return nil, err
	}

	return accounts, nil
}
