package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"go.uber.org/zap"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"

	"gitlab.com/zalleon/payment-system/internal/domain"
)

type putMoneyCommand struct {
	AccountID uuid.UUID
	Amount    decimal.Decimal
}

func NewPutMoneyCommand(accountID string, amount decimal.Decimal) (*putMoneyCommand, *domain.ValidationError) {
	if err := validation.Validate(
		accountID,
		validation.Required,
		is.UUIDv4,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "account",
			Message: err.Error(),
		}
	}

	id, err := uuid.FromString(accountID)
	if err != nil {
		return nil, &domain.ValidationError{
			Field:   "account",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		amount,
		validation.Required,
		validation.By(func(value interface{}) error {
			if v, ok := value.(decimal.Decimal); !ok || v.LessThanOrEqual(decimal.NewFromInt(0)) {
				return fmt.Errorf("should be greater than 0")
			}
			return nil
		}),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "amount",
			Message: err.Error(),
		}
	}

	return &putMoneyCommand{
		AccountID: id,
		Amount:    amount,
	}, nil
}

type PutMoneyCommandHandler struct {
	logger                *zap.Logger
	txManager             TxManagerInterface
	accountRepository     domain.AccountRepositoryInterface
	transactionRepository domain.TransactionRepositoryInterface
}

func NewPutMoneyCommandHandler(
	logger *zap.Logger,
	accountRepository domain.AccountRepositoryInterface,
	transactionRepository domain.TransactionRepositoryInterface,
	txManager TxManagerInterface,
) *PutMoneyCommandHandler {
	return &PutMoneyCommandHandler{
		logger:                logger,
		accountRepository:     accountRepository,
		transactionRepository: transactionRepository,
		txManager:             txManager,
	}
}

func (h *PutMoneyCommandHandler) Handle(ctx context.Context, cmd *putMoneyCommand) error {
	tx, err := h.txManager.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to begin put money transaction.")
	}

	// Put transaction into context
	ctx = WithTransaction(ctx, tx)

	defer func(ctx context.Context, tx TxInterface) {
		// Rollback transaction if not committed
		if tx.IsCommit() {
			return
		}
		if errRollback := tx.Rollback(ctx); errRollback != nil {
			h.logger.Error(errRollback.Error())
		}
	}(ctx, tx)

	// Get account for update
	account, err := h.accountRepository.Get(ctx, cmd.AccountID)
	if err != nil {
		if err == domain.ErrEntityNotFound {
			return &domain.ValidationError{
				Field:   "account",
				Message: "does not exist",
			}
		}
		return errors.Wrapf(err, "unable to get for update account %s.", cmd.AccountID.String())
	}

	account.Balance, err = account.Balance.Add(money.NewMoney(cmd.Amount, account.Balance.Currency()))
	if err != nil {
		return errors.Wrapf(err, "unable to add money")
	}

	// Update account balance
	if err = h.accountRepository.UpdateBalance(ctx, account); err != nil {
		return errors.Wrapf(err, "unable to update account %s balance %v %s.", account.ID.String(), account.Balance.Amount(), account.Balance.Currency().Code)
	}

	id, err := uuid.NewV4()
	if err != nil {
		return errors.Wrap(err, "unable to generate uuid for put money transaction.")
	}

	// Create transaction record
	transaction := &domain.Transaction{
		ID:               id,
		Date:             time.Now(),
		Type:             "put", // TODO: move to constant
		FromAccountID:    account.ID,
		FromCurrency:     account.Balance.Currency().Code,
		FromCurrencyRate: decimal.NewFromInt(1),
		FromAmount:       cmd.Amount,
		ToAccountID:      account.ID,
		ToCurrency:       account.Balance.Currency().Code,
		ToCurrencyRate:   decimal.NewFromInt(1),
		ToAmount:         cmd.Amount,
	}

	// Persist transaction
	if err := h.transactionRepository.Persist(ctx, transaction); err != nil {
		return errors.Wrapf(err, "unable to persist put money transaction %+v", transaction)
	}

	// Commit
	if err := tx.Commit(ctx); err != nil {
		return errors.Wrap(err, "unable to commit put money transaction.")
	}

	return nil
}
