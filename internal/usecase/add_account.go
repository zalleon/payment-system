package usecase

import (
	"context"
	"regexp"

	"gitlab.com/zalleon/payment-system/pkg/money"

	"github.com/shopspring/decimal"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"gitlab.com/zalleon/payment-system/internal/domain"
)

type addAccountCommand struct {
	User     uuid.UUID
	Currency string
}

func NewAddAccountCommand(user string, currency string) (*addAccountCommand, *domain.ValidationError) {
	if err := validation.Validate(
		user,
		validation.Required,
		is.UUIDv4,
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "user",
			Message: err.Error(),
		}
	}

	userID, err := uuid.FromString(user)
	if err != nil {
		return nil, &domain.ValidationError{
			Field:   "user",
			Message: err.Error(),
		}
	}

	if err := validation.Validate(
		currency,
		validation.Required,
		validation.Match(regexp.MustCompile("^[A-Z]{3}$")),
	); err != nil {
		return nil, &domain.ValidationError{
			Field:   "currency",
			Message: err.Error(),
		}
	}

	return &addAccountCommand{
		User:     userID,
		Currency: currency,
	}, nil
}

type AddAccountCommandHandler struct {
	userRepository    domain.UserRepositoryInterface
	accountRepository domain.AccountRepositoryInterface
}

func NewAddAccountCommandHandler(userRepository domain.UserRepositoryInterface, accountRepository domain.AccountRepositoryInterface) *AddAccountCommandHandler {
	return &AddAccountCommandHandler{userRepository: userRepository, accountRepository: accountRepository}
}

func (h *AddAccountCommandHandler) Handle(ctx context.Context, cmd *addAccountCommand) error {
	curr, err := money.GetCurrency(cmd.Currency)
	if err != nil {
		return errors.Wrapf(err, "unable to get currency %s", cmd.Currency)
	}
	// Check user exists
	if _, errGetUser := h.userRepository.Get(ctx, cmd.User); errGetUser != nil {
		if errGetUser == domain.ErrEntityNotFound {
			return &domain.ValidationError{
				Field:   "user",
				Message: "does not exist",
			}
		}
		return errors.Wrapf(errGetUser, "unable to get user %s", cmd.User.String())
	}

	id, err := uuid.NewV4()
	if err != nil {
		return errors.Wrap(err, "unable to generate uuid fot new account.")
	}
	account := &domain.Account{
		ID:      id,
		User:    cmd.User,
		Balance: money.NewMoney(decimal.NewFromInt(0), curr),
	}

	// Persist entity
	if err := h.accountRepository.Persist(ctx, account); err != nil {
		return errors.Wrapf(err, "unable to persist account %+v", account)
	}
	return nil
}
