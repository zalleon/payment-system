// Package usecase ...
package usecase

import (
	"context"
)

type ContextTransactionKey struct{}

//go:generate mockery --output ./../persistence/mocks --name TxManagerInterface --structname TxManagerMock --filename tx_manager_mock.go

// TxManagerInterface ...
type TxManagerInterface interface {
	Begin(ctx context.Context) (TxInterface, error)
}

//go:generate mockery --output ./../persistence/mocks --name TxInterface --structname TxMock --filename tx_mock.go

// TxInterface ...
type TxInterface interface {
	IsCommit() bool
	Commit(ctx context.Context) error
	Rollback(ctx context.Context) error
	Conn() interface{}
}

func WithTransaction(ctx context.Context, tx TxInterface) context.Context {
	return context.WithValue(ctx, ContextTransactionKey{}, tx.Conn())
}
