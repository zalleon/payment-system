package usecase

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

// TransactionDto represents the transaction
//
// swagger:model Transaction
type TransactionDto struct {
	ID        string `json:"id"`
	AccountID string `json:"account_id"`
}

type getTransactionsQuery struct{}

func NewGetTransactionsQuery() *getTransactionsQuery {
	return &getTransactionsQuery{}
}

type GetTransactionsQueryHandler struct {
	db *pgxpool.Pool
}

func NewGetTransactionsQueryHandler(db *pgxpool.Pool) *GetTransactionsQueryHandler {
	return &GetTransactionsQueryHandler{db: db}
}

func (h *GetTransactionsQueryHandler) Handle(ctx context.Context, query *getTransactionsQuery) ([]TransactionDto, error) {
	report := make([]TransactionDto, 0)

	return report, nil

	/**
	userName := string(ctx.QueryArgs().Peek("name"))
	if userName == "" {
		ctx.Error("The field user is mandatory", fasthttp.StatusBadRequest)
		return
	}

	var userID uuid.UUID
	err := h.dbConnectionPool.QueryRow(ctx, "SELECT id FROM users WHERE name=$1", userName).Scan(&userID)
	if err != nil {
		if err == pgx.ErrNoRows {
			ctx.Error("user not found", fasthttp.StatusNotFound)
			return
		}
		ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
		return
	}

	query := "SELECT id,date,type,from_user_id,from_currency,from_currency_rate,from_amount,to_user_id,to_currency,to_currency_rate,to_amount FROM user_transactions WHERE (from_user_id=$1 OR to_user_id=$1)"
	params := make([]interface{}, 0)
	params = append(params, userID.String())

	startAt := string(ctx.QueryArgs().Peek("start_at"))
	endAt := string(ctx.QueryArgs().Peek("end_at"))

	if startAt != "" && endAt != "" {
		startAtParsed, err := time.Parse("2006-01-02", startAt)
		if err != nil {
			ctx.Error("Invalid start_at date format. Required YYYY-mm-dd.", fasthttp.StatusBadRequest)
			return
		}
		endAtParsed, err := time.Parse("2006-01-02", endAt)
		if err != nil {
			ctx.Error("Invalid end_at date format. Required YYYY-mm-dd.", fasthttp.StatusBadRequest)
			return
		}
		if startAtParsed.Unix() > endAtParsed.Unix() {
			ctx.Error("The field start_at should be less or equal end_at", fasthttp.StatusBadRequest)
			return
		}

		query += " AND date BETWEEN $2 AND $3"
		params = append(params, startAt, endAt)
	} else if startAt != "" {
		_, err := time.Parse("2006-01-02", startAt)
		if err != nil {
			ctx.Error("Invalid start_at date format. Required YYYY-mm-dd.", fasthttp.StatusBadRequest)
			return
		}
		query += " AND date >= $2"
		params = append(params, startAt)

	} else if endAt != "" {
		_, err := time.Parse("2006-01-02", endAt)
		if err != nil {
			ctx.Error("Invalid end_at date format. Required YYYY-mm-dd.", fasthttp.StatusBadRequest)
			return
		}

		query += " AND date <= $2"
		params = append(params, endAt)
	}

	rows, err := h.dbConnectionPool.Query(
		ctx,
		query,
		params...,
	)
	if err != nil {
		ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var total, totalUsd decimal.Decimal
	var rawTransactions domain.Transactions
	var transactions []TransactionDto
	for rows.Next() {
		var row domain.Transaction
		var transaction TransactionDto
		err = rows.Scan(&row.ID, &row.Date, &row.Type, &row.FromAccountID, &row.FromCurrency, &row.FromCurrencyRate, &row.FromAmount, &row.ToAccountID, &row.ToCurrency, &row.ToCurrencyRate, &row.ToAmount)
		if err != nil {
			h.logger.Error(fmt.Sprintf("Unable to scan row: %v", err))

			ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
			return
		}
		transaction.ID = row.ID.String()
		transaction.Date = row.Date.Format("2006-01-02")
		transaction.Type = row.Type

		switch row.Type {
		case "put":
			transaction.Amount = row.ToAmount
			transaction.AmountUsd = row.ToAmount.DivRound(row.ToCurrencyRate, 2)
			transaction.Currency = row.ToCurrency

			total = total.Add(transaction.Amount)
			totalUsd = totalUsd.Add(transaction.AmountUsd)
		case "transfer":
			if userID == row.ToAccountID {
				transaction.Amount = row.ToAmount
				transaction.AmountUsd = row.ToAmount.DivRound(row.ToCurrencyRate, 2)

				total = total.Add(transaction.Amount)
				totalUsd = totalUsd.Add(transaction.AmountUsd)

				transaction.Currency = row.ToCurrency
			}
			if userID == row.FromAccountID {
				transaction.Amount = row.FromAmount
				transaction.AmountUsd = row.FromAmount.DivRound(row.FromCurrencyRate, 2)

				total = total.Sub(transaction.Amount)
				totalUsd = totalUsd.Sub(transaction.AmountUsd)

				transaction.Amount = transaction.Amount.Neg()
				transaction.AmountUsd = transaction.AmountUsd.Neg()

				transaction.Currency = row.FromCurrency
			}
		}

		rawTransactions = append(rawTransactions, row)
		transactions = append(transactions, transaction)
	}
	if rows.Err() != nil {
		h.logger.Error(fmt.Sprintf("Rows error: %v", rows.Err()))

		ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
		return
	}

	resp := GetUserTransactionsResponse{
		User: userName,
		//RawTransactions: rawTransactions,
		Transactions: transactions,
		Total:        total,
		TotalUsd:     totalUsd,
	}

	format := string(ctx.QueryArgs().Peek("format"))
	if format == "xml" {
		xmlResponse, err := xml.Marshal(resp)
		if err != nil {
			h.logger.Error(fmt.Sprintf("Unable to marshal response: %v error: %v", resp, err))

			ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
			return
		}

		ctx.Response.Header.Add("Content-Description", "File Transfer")
		ctx.Response.Header.Add("Content-Disposition", "attachment; filename=transactions.xml")
		ctx.Response.Header.Set("Content-Type", "application/xml")
		fmt.Fprint(ctx, string(xmlResponse))
	} else {
		jsonResponse, err := json.Marshal(resp)
		if err != nil {
			h.logger.Error(fmt.Sprintf("Unable to marshal response: %v error: %v", resp, err))

			ctx.Error("Internal server error", fasthttp.StatusInternalServerError)
			return
		}

		ctx.Response.Header.Set("Content-Type", "application/json")
		fmt.Fprint(ctx, string(jsonResponse))
	}
	*/
}
