package money

import "testing"

func TestCurrency_Get(t *testing.T) {
	tcs := []struct {
		code     string
		expected string
	}{
		{"EUR", "EUR"},
		{"Eur", "EUR"},
	}

	for _, tc := range tcs {
		c, err := GetCurrency(tc.code)
		if err != nil {
			t.Errorf("unable to get currency by code [%s]", tc.code)
		} else if c.Code != tc.expected {
			t.Errorf("Expected %s got %s", tc.expected, c.Code)
		}
	}
}

func TestCurrency_GetUndefined(t *testing.T) {
	if _, err := GetCurrency("UNDEFINED"); err == nil {
		t.Error("Expect currency not found error")
	}
}

func TestCurrency_Equals(t *testing.T) {
	tcs := []struct {
		code  string
		other string
	}{
		{"EUR", "EUR"},
		{"Eur", "EUR"},
		{"usd", "USD"},
	}

	for _, tc := range tcs {
		c, err := GetCurrency(tc.code)
		if err != nil {
			t.Errorf("unable to get currency by code [%s]", tc.code)
			continue
		}

		oc, err := GetCurrency(tc.other)
		if err != nil {
			t.Errorf("unable to get currency by code [%s]", tc.other)
			continue
		}

		if !c.Equal(oc) {
			t.Errorf("Expected that %v is not equal %v", c, oc)
		}
	}
}
