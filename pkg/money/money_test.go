package money

import (
	"encoding/json"
	"testing"

	"github.com/shopspring/decimal"
)

func TestMoney_New(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	if !m.Amount().Equal(decimal.NewFromInt(1)) {
		t.Errorf("Expected %d got %s", 1, m.Amount().String())
	}

	if m.Currency().Code != CodeEUR {
		t.Errorf("Expected currency %s got %s", c.Code, m.Currency().Code)
	}
}

func TestMoney_SameCurrency(t *testing.T) {
	eur, _ := GetCurrency(CodeEUR)
	usd, _ := GetCurrency(CodeUSD)

	mEUR := NewMoney(decimal.NewFromInt(10), eur)
	mUSD := NewMoney(decimal.NewFromInt(10), usd)

	if mEUR.SameCurrency(mUSD) {
		t.Errorf("Expected %s not to be same as %s", mEUR.Currency().Code, mUSD.Currency().Code)
	}

	omEUR := NewMoney(decimal.NewFromInt(10), eur)

	if !mEUR.SameCurrency(omEUR) {
		t.Errorf("Expected %s to be same as %s", mEUR.Currency().Code, omEUR.Currency().Code)
	}
}

func TestMoney_Equal(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(0), false},
		{decimal.NewFromInt32(1), true},
		{decimal.NewFromInt32(2), false},
	}

	for _, tc := range tcs {
		om := NewMoney(tc.amount, c)

		r, err := m.Equals(om)
		if err != nil || r != tc.expected {
			t.Errorf("Expected %s Equals %s == %t got %t", m.Amount().String(), om.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_GreaterThan(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(0), true},
		{decimal.NewFromInt32(1), false},
		{decimal.NewFromInt32(2), false},
	}

	for _, tc := range tcs {
		om := NewMoney(tc.amount, c)

		r, err := m.GreaterThan(om)
		if err != nil || r != tc.expected {
			t.Errorf("Expected %s Equals %s == %t got %t", m.Amount().String(), om.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_GreaterThanOrEqual(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(0), true},
		{decimal.NewFromInt32(1), true},
		{decimal.NewFromInt32(2), false},
	}

	for _, tc := range tcs {
		om := NewMoney(tc.amount, c)

		r, err := m.GreaterThanOrEqual(om)
		if err != nil || r != tc.expected {
			t.Errorf("Expected %s Equals %s == %t got %t", m.Amount().String(), om.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_LessThan(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(0), false},
		{decimal.NewFromInt32(1), false},
		{decimal.NewFromInt32(2), true},
	}

	for _, tc := range tcs {
		om := NewMoney(tc.amount, c)

		r, err := m.LessThan(om)
		if err != nil || r != tc.expected {
			t.Errorf("Expected %s Equals %s == %t got %t", m.Amount().String(), om.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_LessThanOrEqual(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	m := NewMoney(decimal.NewFromInt(1), c)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(0), false},
		{decimal.NewFromInt32(1), true},
		{decimal.NewFromInt32(2), true},
	}

	for _, tc := range tcs {
		om := NewMoney(tc.amount, c)

		r, err := m.LessThanOrEqual(om)
		if err != nil || r != tc.expected {
			t.Errorf("Expected %s Equals %s == %t got %t", m.Amount().String(), om.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_IsZero(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(-1), false},
		{decimal.NewFromInt32(0), true},
		{decimal.NewFromInt32(1), false},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount, c)

		r := m.IsZero()
		if r != tc.expected {
			t.Errorf("Expected %s to be zero == %t got %t", m.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_IsNegative(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(-1), true},
		{decimal.NewFromInt32(0), false},
		{decimal.NewFromInt32(1), false},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount, c)

		r := m.IsNegative()
		if r != tc.expected {
			t.Errorf("Expected %s to be negative == %t got %t", m.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_IsPositive(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)

	tcs := []struct {
		amount   decimal.Decimal
		expected bool
	}{
		{decimal.NewFromInt32(-1), false},
		{decimal.NewFromInt32(0), false},
		{decimal.NewFromInt32(1), true},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount, c)

		r := m.IsPositive()
		if r != tc.expected {
			t.Errorf("Expected %s to be positive == %t got %t", m.Amount().String(), tc.expected, r)
		}
	}
}

func TestMoney_Add(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	tcs := []struct {
		amount1  decimal.Decimal
		amount2  decimal.Decimal
		expected decimal.Decimal
	}{
		{decimal.NewFromInt32(5), decimal.NewFromInt32(5), decimal.NewFromInt32(10)},
		{decimal.NewFromInt32(10), decimal.NewFromInt32(5), decimal.NewFromInt32(15)},
		{decimal.NewFromInt32(1), decimal.NewFromInt32(-1), decimal.NewFromInt32(0)},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount1, c)
		om := NewMoney(tc.amount2, c)

		r, err := m.Add(om)
		if err != nil {
			t.Error(err)
		}

		if !r.Amount().Equal(tc.expected) {
			t.Errorf("Expected %s + %s = %s got %s", tc.amount1.String(), tc.amount2.String(),
				tc.expected.String(), r.Amount().String())
		}
	}
}

func TestMoney_AddWithDiffCurrency(t *testing.T) {
	eur, _ := GetCurrency(CodeEUR)
	usd, _ := GetCurrency(CodeUSD)

	mEUR := NewMoney(decimal.NewFromInt32(100), eur)
	mUSD := NewMoney(decimal.NewFromInt32(100), usd)

	if r, err := mEUR.Add(mUSD); r != nil || err == nil {
		t.Error("Expected error")
	}
}

func TestMoney_Subtract(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	tcs := []struct {
		amount1  decimal.Decimal
		amount2  decimal.Decimal
		expected decimal.Decimal
	}{
		{decimal.NewFromInt32(5), decimal.NewFromInt32(5), decimal.NewFromInt32(0)},
		{decimal.NewFromInt32(10), decimal.NewFromInt32(5), decimal.NewFromInt32(5)},
		{decimal.NewFromInt32(5), decimal.NewFromInt32(10), decimal.NewFromInt32(-5)},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount1, c)
		om := NewMoney(tc.amount2, c)

		r, err := m.Subtract(om)
		if err != nil {
			t.Error(err)
		}

		if !r.Amount().Equal(tc.expected) {
			t.Errorf("Expected %s + %s = %s got %s", tc.amount1.String(), tc.amount2.String(),
				tc.expected.String(), r.Amount().String())
		}
	}
}

func TestMoney_SubtractWithDiffCurrency(t *testing.T) {
	eur, _ := GetCurrency(CodeEUR)
	usd, _ := GetCurrency(CodeUSD)

	mEUR := NewMoney(decimal.NewFromInt32(100), eur)
	mUSD := NewMoney(decimal.NewFromInt32(100), usd)

	if r, err := mEUR.Subtract(mUSD); r != nil || err == nil {
		t.Error("Expected error")
	}
}

func TestMoney_Multiply(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	tcs := []struct {
		amount     decimal.Decimal
		multiplier decimal.Decimal
		expected   decimal.Decimal
	}{
		{decimal.NewFromInt32(5), decimal.NewFromInt32(5), decimal.NewFromInt32(25)},
		{decimal.NewFromInt32(10), decimal.NewFromInt32(5), decimal.NewFromInt32(50)},
		{decimal.NewFromInt32(1), decimal.NewFromInt32(0), decimal.NewFromInt32(0)},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount, c)

		if r := m.Multiply(tc.multiplier); !r.Amount().Equal(tc.expected) {
			t.Errorf("Expected %s * %s = %s got %s", tc.amount.String(), tc.multiplier.String(),
				tc.expected.String(), r.Amount().String())
		}
	}
}

func TestMoney_Round(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	tcs := []struct {
		places   int32
		amount   decimal.Decimal
		expected decimal.Decimal
	}{
		{2, decimal.NewFromFloat(0), decimal.NewFromFloat(0)},
		{2, decimal.NewFromFloat(125.4501), decimal.NewFromFloat(125.45)},
		{2, decimal.NewFromFloat(125.4571), decimal.NewFromFloat(125.46)},
		{2, decimal.NewFromFloat(-1.458), decimal.NewFromFloat(-1.46)},
	}

	for _, tc := range tcs {
		m := NewMoney(tc.amount, c)

		r := m.Round(tc.places)
		if !r.Amount().Equal(tc.expected) {
			t.Errorf("Expected rounded %s to be %s got %s places %d", tc.amount.String(), tc.expected.String(), r.Amount().String(), tc.places)
		}
	}
}

func TestCustomMarshal(t *testing.T) {
	c, _ := GetCurrency(CodeEUR)
	money := NewMoney(decimal.NewFromFloat(125.45), c)

	expected := `{"amount":125.45,"currency":"EUR"}`

	b, err := json.Marshal(money)
	if err != nil {
		t.Error(err)
	}

	if string(b) != expected {
		t.Errorf("Expected %s got %s", expected, string(b))
	}
}

func TestDefaultUnmarshal(t *testing.T) {
	tcs := []struct {
		json             string
		expectedAmount   string
		expectedCurrency string
		expectedError    string
		expectError      bool
	}{
		{`{"amount": 100.12, "currency":"USD"}`, "100.12", "USD", "", false},
		{`{"amount": "100.12", "currency":"USD"}`, "100.12", "USD", "", false},
		{`{"currency":"USD"}`, "", "", "unable to convert amount", true},
		{`{"amount": 100.12}`, "", "", "unable to convert currency", true},
	}

	for _, tc := range tcs {
		var m Money
		err := json.Unmarshal([]byte(tc.json), &m)
		if tc.expectError {
			if err == nil {
				t.Errorf("Expect error")
			} else if err.Error() != tc.expectedError {
				t.Errorf("Expect error %s actual %s", tc.expectedError, err.Error())
			}
		} else {
			if err != nil {
				t.Errorf("Unexpected error %s", err.Error())
			}

			if m.Amount().String() != tc.expectedAmount {
				t.Errorf("Expected amount %s got %s", tc.expectedAmount, m.Amount().String())
			}

			if m.Currency().Code != tc.expectedCurrency {
				t.Errorf("Expected currency %s got %s", tc.expectedCurrency, m.Amount().String())
			}
		}
	}
}
