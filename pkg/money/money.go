package money

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/shopspring/decimal"
)

// Based on https://github.com/Rhymond/go-money/blob/master/money.go

// Injection points for backward compatibility.
// If you need to keep your JSON marshal/unmarshal way, overwrite them like below.
//   money.UnmarshalJSON = func (m *Money, b []byte) error { ... }
//   money.MarshalJSON = func (m Money) ([]byte, error) { ... }
var (
	// UnmarshalJSONFunc is injection point of json.Unmarshaller for money.Money
	UnmarshalJSON = defaultUnmarshalJSON
	// MarshalJSONFunc is injection point of json.Marshaller for money.Money
	MarshalJSON = defaultMarshalJSON
)

func defaultUnmarshalJSON(m *Money, b []byte) error {
	data := make(map[string]interface{})
	err := json.Unmarshal(b, &data)
	if err != nil {
		return err
	}

	currencyCode, ok := data["currency"].(string)
	if !ok {
		return fmt.Errorf("unable to convert currency")
	}

	currency, err := GetCurrency(currencyCode)
	if err != nil {
		return err
	}

	var amount decimal.Decimal

	if a, ok := data["amount"].(float64); ok {
		amount = decimal.NewFromFloat(a)
	} else if a, ok := data["amount"].(string); ok {
		if da, err := decimal.NewFromString(a); err != nil {
			return err
		} else {
			amount = da
		}
	} else {
		return fmt.Errorf("unable to convert amount")
	}

	ref := NewMoney(amount, currency)
	*m = *ref
	return nil
}

func defaultMarshalJSON(m *Money) ([]byte, error) {
	buff := bytes.NewBufferString(fmt.Sprintf(`{"amount": %v, "currency": "%s"}`, m.Amount(), m.Currency().Code))
	return buff.Bytes(), nil
}

// UnmarshalJSON is implementation of json.Unmarshaller
func (m *Money) UnmarshalJSON(b []byte) error {
	return UnmarshalJSON(m, b)
}

// MarshalJSON is implementation of json.Marshaller
func (m *Money) MarshalJSON() ([]byte, error) {
	return MarshalJSON(m)
}

// Money represents monetary value information, stores currency and amount value.
type Money struct {
	amount   decimal.Decimal
	currency *Currency
}

func (m *Money) Amount() decimal.Decimal {
	return m.amount
}

func (m *Money) Currency() *Currency {
	return m.currency
}

func NewMoney(amount decimal.Decimal, currency *Currency) *Money {
	return &Money{amount: amount, currency: currency}
}

// SameCurrency check if given Money is equals by currency.
func (m *Money) SameCurrency(om *Money) bool {
	return m.Currency().Equal(om.Currency())
}

func (m *Money) assertSameCurrency(om *Money) error {
	if !m.SameCurrency(om) {
		return errors.New("currencies don't match")
	}

	return nil
}

// Equals checks equality between two Money types.
func (m *Money) Equals(om *Money) (bool, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return false, err
	}
	return m.Amount().Equal(om.Amount()), nil
}

// GreaterThan checks whether the value of Money is greater than the other.
func (m *Money) GreaterThan(om *Money) (bool, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return false, err
	}
	return m.Amount().GreaterThan(om.Amount()), nil
}

// GreaterThanOrEqual checks whether the value of Money is greater or equal than the other.
func (m *Money) GreaterThanOrEqual(om *Money) (bool, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return false, err
	}
	return m.Amount().GreaterThanOrEqual(om.Amount()), nil
}

// LessThan checks whether the value of Money is less than the other.
func (m *Money) LessThan(om *Money) (bool, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return false, err
	}

	return m.Amount().LessThan(om.Amount()), nil
}

// LessThanOrEqual checks whether the value of Money is less or equal than the other.
func (m *Money) LessThanOrEqual(om *Money) (bool, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return false, err
	}
	return m.Amount().LessThanOrEqual(om.Amount()), nil
}

// IsZero returns boolean of whether the value of Money is equals to zero.
func (m *Money) IsZero() bool {
	return m.Amount().IsZero()
}

// IsPositive returns boolean of whether the value of Money is positive.
func (m *Money) IsPositive() bool {
	return m.Amount().IsPositive()
}

// IsNegative returns boolean of whether the value of Money is negative.
func (m *Money) IsNegative() bool {
	return m.Amount().IsNegative()
}

// Add returns new Money struct with value representing sum of Self and Other Money.
func (m *Money) Add(om *Money) (*Money, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return nil, err
	}
	return &Money{amount: m.Amount().Add(om.Amount()), currency: m.Currency()}, nil
}

// Subtract returns new Money struct with value representing difference of Self and Other Money.
func (m *Money) Subtract(om *Money) (*Money, error) {
	if err := m.assertSameCurrency(om); err != nil {
		return nil, err
	}
	return &Money{amount: m.Amount().Sub(om.Amount()), currency: m.Currency()}, nil
}

// Multiply returns new Money struct with value representing Self multiplied value by multiplier.
func (m *Money) Multiply(mul decimal.Decimal) *Money {
	return &Money{amount: m.Amount().Mul(mul), currency: m.Currency()}
}

// Round returns new Money struct with value rounded to nearest zero.
func (m *Money) Round(places int32) *Money {
	return &Money{amount: m.Amount().Round(places), currency: m.Currency()}
}
