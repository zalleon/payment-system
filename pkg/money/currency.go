// Package money represents money pattern realisation
// https://martinfowler.com/eaaCatalog/money.html
// Based on https://github.com/Rhymond/go-money/blob/master/currency.go
package money

import (
	"fmt"
	"strings"
)

const CodeUSD = "USD"
const CodeEUR = "EUR"
const CodeRUB = "RUB"

type Currency struct {
	Code     string
	Fraction int
	Grapheme string
	Template string
	Decimal  string
	Thousand string
}

var currencies = map[string]Currency{
	CodeUSD: {Decimal: ".", Thousand: ",", Code: "USD", Fraction: 2, Grapheme: "$", Template: "$1"},
	CodeEUR: {Decimal: ".", Thousand: ",", Code: "EUR", Fraction: 2, Grapheme: "\u20ac", Template: "$1"},
	CodeRUB: {Decimal: ".", Thousand: ",", Code: "RUB", Fraction: 2, Grapheme: "\u20bd", Template: "1 $"},
}

// GetCurrency returns the currency given the code.
func GetCurrency(code string) (*Currency, error) {
	code = strings.ToUpper(code)
	if c, ok := currencies[code]; !ok {
		return nil, fmt.Errorf("currency [%s] not found", code)
	} else {
		return &c, nil
	}
}

func (c *Currency) Equal(oc *Currency) bool {
	return c.Code == oc.Code
}
