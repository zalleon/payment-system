GITREV?=$(shell git log -1 --pretty=format:"%H" || echo undefined)
DATE?=$(shell date -u "+%Y-%m-%d %H:%M:%S")
LDFLAGS="-s -extldflags '-static' \
	-X 'main.AppVersion=${IMAGE_TAG}' \
	-X 'main.GitRev=${GITREV}' \
	-X 'main.BuildDate=${DATE}'"

OUTPUT?=./bin/server

.PHONY: deps
deps:
	@echo "+ $@"
	GO111MODULE=on go mod download && go mod tidy
	GO111MODULE=on go mod vendor

.PHONY: check-linter
check-linter:
ifeq (, $(shell which golangci-lint))
	$(error "No golangci-lint in $(PATH). Install it from https://github.com/golangci/golangci-lint")
endif

.PHONY: lint
lint: check-linter
	golangci-lint run --fix

.PHONY: test
test:
ifeq ($(ENVIRONMENT), ci)
	@echo "+ $@"
	GO111MODULE=on go test -mod=vendor -cover ./... -coverprofile cover.out
else
	@echo "+ $@"
	GO111MODULE=on go test -cover ./... -coverprofile cover.out
endif

# https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing
.PHONY: print-test-cover
print-test-cover:
	@echo "+ $@"
	GO111MODULE=on go tool cover -func cover.out | grep total | awk '{print $3}'

.PHONY: test-integration
test-integration:
	@echo "+ $@"
	GO111MODULE=on go test -cover -tags=integration ./...

.PHONY: clean
clean:
	rm -rf ${OUTPUT}

.PHONY: build
build: clean
ifndef IMAGE_TAG
	IMAGE_TAG=v0.1.0-dev $(MAKE) build
else
	@echo "+ $@"
	GO111MODULE=on go build \
		-mod vendor \
		-tags "netgo std static_all" \
		-gcflags -m \
		-ldflags $(LDFLAGS) \
		-o ${OUTPUT} ./cmd/server/main.go
endif

.PHONY: check-swagger
check-swagger:
ifeq (, $(shell which swagger))
	$(error "No swagger in $(PATH). Install it from https://goswagger.io/install.html")
endif

.PHONY: swagger
swagger: check-swagger
	swagger generate spec -o ./docs/swagger.json --scan-models

.PHONY: check-runner
check-runner:
ifeq (, $(shell which gitlab-runner))
	$(error "No gitlab-runner in $(PATH). Install it from https://docs.gitlab.com/runner/install/")
endif

.PHONY: ci-lint
ci-lint: check-runner
	ENVIRONMENT=ci gitlab-runner exec docker lint

.PHONY: ci-test
ci-test: check-runner
	ENVIRONMENT=ci gitlab-runner exec docker test

.PHONY: ci-build
ci-build: check-runner
	ENVIRONMENT=ci gitlab-runner exec docker compile