FROM golang:1.14-alpine

RUN mkdir /payment-system
ADD . /payment-system

WORKDIR /payment-system

RUN ls
RUN go build -mod vendor -tags "netgo std static_all" -o api cmd/server/main.go

EXPOSE 8080

CMD ["/payment-system/api"]